ECL_TOPDIR = /usr
ECL = $(ECL_TOPDIR)/bin/ecl
LIBJVMDIR = $(wildcard /usr/lib*/jvm/java/jre/lib/*/server)
JVMINCLUDEDIR = $(wildcard /usr/lib*/jvm/java/include)
CFLAGS = -O2 -fPIC
export ECL_INCLUDEDIR = $(ECL_TOPDIR)/include/
export CFLAGS += -I$(ECL_INCLUDEDIR) -I$(JVMINCLUDEDIR) -I$(JVMINCLUDEDIR)/linux

all: 64bit

32bit:
	$(MAKE) EXTRA_CFLAGS=-DINT_POINTER ECL_LIBDIR="$(ECL_TOPDIR)/lib" LDFLAGS="-L$(ECL_TOPDIR)/lib" libjffi.so jffi.fas
	$(MAKE) -C jffi 32bit

64bit:
	$(MAKE) EXTRA_CFLAGS=-DLONG_POINTER ECL_LIBDIR="$(ECL_TOPDIR)/lib64" LDFLAGS="-L$(ECL_TOPDIR)/lib64" libjffi.so jffi.fas
	$(MAKE) -C jffi 64bit

libjffi.so: libjffi_lisp.a jffi_c_layer.o jffi_Lisp_start.o jffi_Java_pseudo_start.o
	$(CC) -shared -o libjffi.so $(LDFLAGS) jffi_c_layer.o jffi_Lisp_start.o jffi_Java_pseudo_start.o libjffi_lisp.a -lecl

libjffi_lisp.a: jffi.lisp jffi_c_layer.h
	$(ECL) --shell compile.lisp

jffi_c_layer.o: jffi_c_layer.c jffi_c_layer.h
	$(CC) -Wall -std=gnu99 $(EXTRA_CFLAGS) $(CFLAGS) -c jffi_c_layer.c

jffi_Lisp_start.o: jffi_Lisp_start.c jffi_c_layer.h
	$(CC) -Wall -std=gnu99 $(EXTRA_CFLAGS) $(CFLAGS) -c jffi_Lisp_start.c

jffi_Java_start.o: jffi_Java_start.c jffi_c_layer.h
	$(CC) -Wall -std=gnu99 $(EXTRA_CFLAGS) $(CFLAGS) -c jffi_Java_start.c

jffi_Java_pseudo_start.o: jffi_Java_pseudo_start.c jffi_c_layer.h
	$(CC) -Wall -std=gnu99 $(EXTRA_CFLAGS) $(CFLAGS) -c jffi_Java_pseudo_start.c

jffi.fas: libjffi_lisp.a jffi_c_layer.o jffi_Java_start.o
	LIBJVMDIR=$(LIBJVMDIR) ecl --shell compile-fasl.lisp

clean:
	rm -f libjffi.a libjffi_lisp.a *.o libjffi.so jffi.fas
	$(MAKE) -C jffi clean
