;; Copyright 2017 Marius Gerbershagen
;; This file is part of JFFI.
;;
;;  JFFI is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU Lesser General Public License as
;;  published by the Free Software Foundation, either version 3 of the
;;  License, or (at your option) any later version.
;;
;;  JFFI is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU Lesser General Public License for more details.
;;
;;  You should have received a copy of the GNU Lesser General Public
;;  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.
(defpackage #:jffi
  (:use #:common-lisp)
  (:export def-class instanceof make-null null-p java-eq
	   java-exception get-java-exception
	   java-exception-message java-exception-to-string
	   new-array array-elt fill-array array-length
	   convert-to-jarray convert-from-jarray
	   convert-to-jstring convert-from-jstring
	   attach-current-thread detach-current-thread
	   java-environment-error is-running
	   create-java-vm destroy-java-vm
	   unref unref-local with-unref with-unref-local
	   as-global-reference as-local-reference)
  (:documentation "Fast, lightweight, bidirectional Java-Lisp foreign function interface using Embeddable Common Lisp and the Java Native Interface"))
(in-package #:jffi)

#+ecl (ffi:clines "#include \"jffi_c_layer.h\"")

;; *******************************************************
;; *            Definitions for calls TO Java            *
;; *******************************************************
;; mapping of java types to c types
(ffi:def-foreign-type jboolean :uint8-t)
(ffi:def-foreign-type jbyte :int8-t)
(ffi:def-foreign-type jchar :uint16-t)
(ffi:def-foreign-type jshort :int16-t)
(ffi:def-foreign-type jint :int32-t)
(ffi:def-foreign-type jlong :int64-t)
(ffi:def-foreign-type jfloat :float)
(ffi:def-foreign-type jdouble :double)
;; This is not the same type as e.g. a jbooleanArray in c-code, but rather
;; a c-array of jbooleans
(ffi:def-array-pointer jboolean-array jboolean)
(ffi:def-array-pointer jbyte-array jbyte)
(ffi:def-array-pointer jchar-array jchar)
(ffi:def-array-pointer jshort-array jshort)
(ffi:def-array-pointer jint-array jint)
(ffi:def-array-pointer jlong-array jlong)
(ffi:def-array-pointer jfloat-array jfloat)
(ffi:def-array-pointer jdouble-array jdouble)
;; arbitary java functions are called with jvalues as arguments
(ffi:def-union jvalue
  (z jboolean)
  (b jbyte)
  (c jchar)
  (s jshort)
  (i jint)
  (j jlong)
  (f jfloat)
  (d jdouble)
  (l :pointer-void))

;; ***** Definitions of c functions wrapping bare JNI calls *****
(ffi:def-function "java_attach_current_thread" () :returning jint)
(ffi:def-function "java_detach_current_thread" () :returning jint)
(ffi:def-function "java_create_java_vm" ((n_options jint) (options (* :cstring))) :returning jint)
(ffi:def-function "java_destroy_java_vm" () :returning jint)
(ffi:def-function "is_jvm_running" () :returning jboolean)

(ffi:def-function "jvalue_array_pointer" ((array (* 'jvalue)) (n :unsigned-int)) :returning (* 'jvalue))
(ffi:def-function "java_test_exception" () :returning :pointer-void)
(ffi:def-function "signal_lisp_condition" ((exception :pointer-void)) :returning :void)

(ffi:def-function "java_is_same_object" ((obj1 :pointer-void) (obj2 :pointer-void)) :returning jboolean)
(ffi:def-function "java_new_global_ref" ((obj :pointer-void)) :returning :pointer-void)
(ffi:def-function "java_delete_global_ref" ((obj :pointer-void)) :returning :void)
(ffi:def-function "java_new_local_ref" ((obj :pointer-void)) :returning :pointer-void)
(ffi:def-function "java_delete_local_ref" ((obj :pointer-void)) :returning :void)
(ffi:def-function "java_jclass" ((class :cstring)) :returning :pointer-void)
(ffi:def-function "java_jmethodID" ((class :pointer-void) (name :cstring) (signature :cstring)) :returning :pointer-void)
(ffi:def-function "java_static_jmethodID" ((class :pointer-void) (name :cstring) (signature :cstring)) :returning :pointer-void)
(ffi:def-function "java_jfieldID" ((class :pointer-void) (name :cstring) (signature :cstring)) :returning :pointer-void)
(ffi:def-function "java_static_jfieldID" ((class :pointer-void) (name :cstring) (signature :cstring)) :returning :pointer-void)
(ffi:def-function "java_constructorID" ((class :pointer-void) (signature :cstring)) :returning :pointer-void)

(ffi:def-function "java_constructor" ((class :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning :pointer-void)

(ffi:def-function "java_void_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning :void)
(ffi:def-function "java_object_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning :pointer-void)
(ffi:def-function "java_boolean_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jboolean)
(ffi:def-function "java_byte_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jbyte)
(ffi:def-function "java_char_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jchar)
(ffi:def-function "java_short_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jshort)
(ffi:def-function "java_int_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jint)
(ffi:def-function "java_long_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jlong)
(ffi:def-function "java_float_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jfloat)
(ffi:def-function "java_double_method" ((jobject :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jdouble)

(ffi:def-function "java_static_void_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning :void)
(ffi:def-function "java_static_object_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning :pointer-void)
(ffi:def-function "java_static_boolean_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jboolean)
(ffi:def-function "java_static_byte_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jbyte)
(ffi:def-function "java_static_char_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jchar)
(ffi:def-function "java_static_short_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jshort)
(ffi:def-function "java_static_int_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jint)
(ffi:def-function "java_static_long_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jlong)
(ffi:def-function "java_static_float_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jfloat)
(ffi:def-function "java_static_double_method" ((jclass :pointer-void) (jmethodID :pointer-void) (args (* 'jvalue))) :returning jdouble)

(ffi:def-function "java_object_field" ((jobject :pointer-void) (jfieldID :pointer-void)) :returning :pointer-void)
(ffi:def-function "java_boolean_field" ((jobject :pointer-void) (jfieldID :pointer-void)) :returning jboolean)
(ffi:def-function "java_byte_field" ((jobject :pointer-void) (jfieldID :pointer-void)) :returning jbyte)
(ffi:def-function "java_char_field" ((jobject :pointer-void) (jfieldID :pointer-void)) :returning jchar)
(ffi:def-function "java_short_field" ((jobject :pointer-void) (jfieldID :pointer-void)) :returning jshort)
(ffi:def-function "java_int_field" ((jobject :pointer-void) (jfieldID :pointer-void)) :returning jint)
(ffi:def-function "java_long_field" ((jobject :pointer-void) (jfieldID :pointer-void)) :returning jlong)
(ffi:def-function "java_float_field" ((jobject :pointer-void) (jfieldID :pointer-void)) :returning jfloat)
(ffi:def-function "java_double_field" ((jobject :pointer-void) (jfieldID :pointer-void)) :returning jdouble)

(ffi:def-function "java_static_object_field" ((jclass :pointer-void) (jfieldID :pointer-void)) :returning :pointer-void)
(ffi:def-function "java_static_boolean_field" ((jclass :pointer-void) (jfieldID :pointer-void)) :returning jboolean)
(ffi:def-function "java_static_byte_field" ((jclass :pointer-void) (jfieldID :pointer-void)) :returning jbyte)
(ffi:def-function "java_static_char_field" ((jclass :pointer-void) (jfieldID :pointer-void)) :returning jchar)
(ffi:def-function "java_static_short_field" ((jclass :pointer-void) (jfieldID :pointer-void)) :returning jshort)
(ffi:def-function "java_static_int_field" ((jclass :pointer-void) (jfieldID :pointer-void)) :returning jint)
(ffi:def-function "java_static_long_field" ((jclass :pointer-void) (jfieldID :pointer-void)) :returning jlong)
(ffi:def-function "java_static_float_field" ((jclass :pointer-void) (jfieldID :pointer-void)) :returning jfloat)
(ffi:def-function "java_static_double_field" ((jclass :pointer-void) (jfieldID :pointer-void)) :returning jdouble)

(ffi:def-function "java_set_object_field" ((jobject :pointer-void) (jfieldID :pointer-void) (object :pointer-void)) :returning :void)
(ffi:def-function "java_set_boolean_field" ((jobject :pointer-void) (jfieldID :pointer-void) (object jboolean)) :returning :void)
(ffi:def-function "java_set_byte_field" ((jobject :pointer-void) (jfieldID :pointer-void) (object jbyte)) :returning :void)
(ffi:def-function "java_set_char_field" ((jobject :pointer-void) (jfieldID :pointer-void) (object jchar)) :returning :void)
(ffi:def-function "java_set_short_field" ((jobject :pointer-void) (jfieldID :pointer-void) (object jshort)) :returning :void)
(ffi:def-function "java_set_int_field" ((jobject :pointer-void) (jfieldID :pointer-void) (object jint)) :returning :void)
(ffi:def-function "java_set_long_field" ((jobject :pointer-void) (jfieldID :pointer-void) (object jlong)) :returning :void)
(ffi:def-function "java_set_float_field" ((jobject :pointer-void) (jfieldID :pointer-void) (object jfloat)) :returning :void)
(ffi:def-function "java_set_double_field" ((jobject :pointer-void) (jfieldID :pointer-void) (object jdouble)) :returning :void)

(ffi:def-function "java_set_static_object_field" ((jclass :pointer-void) (jfieldID :pointer-void) (object :pointer-void)) :returning :void)
(ffi:def-function "java_set_static_boolean_field" ((jclass :pointer-void) (jfieldID :pointer-void) (object jboolean)) :returning :void)
(ffi:def-function "java_set_static_byte_field" ((jclass :pointer-void) (jfieldID :pointer-void) (object jbyte)) :returning :void)
(ffi:def-function "java_set_static_char_field" ((jclass :pointer-void) (jfieldID :pointer-void) (object jchar)) :returning :void)
(ffi:def-function "java_set_static_short_field" ((jclass :pointer-void) (jfieldID :pointer-void) (object jshort)) :returning :void)
(ffi:def-function "java_set_static_int_field" ((jclass :pointer-void) (jfieldID :pointer-void) (object jint)) :returning :void)
(ffi:def-function "java_set_static_long_field" ((jclass :pointer-void) (jfieldID :pointer-void) (object jlong)) :returning :void)
(ffi:def-function "java_set_static_float_field" ((jclass :pointer-void) (jfieldID :pointer-void) (object jfloat)) :returning :void)
(ffi:def-function "java_set_static_double_field" ((jclass :pointer-void) (jfieldID :pointer-void) (object jdouble)) :returning :void)

(ffi:def-function "array_length" ((array :pointer-void)) :returning jint)
(ffi:def-function "java_new_object_array" ((size jint) (jclass :pointer-void) (initial-element :pointer-void)) :returning :pointer-void)
(ffi:def-function "java_new_boolean_array" ((size jint)) :returning :pointer-void)
(ffi:def-function "java_new_byte_array" ((size jint)) :returning :pointer-void)
(ffi:def-function "java_new_char_array" ((size jint)) :returning :pointer-void)
(ffi:def-function "java_new_short_array" ((size jint)) :returning :pointer-void)
(ffi:def-function "java_new_int_array" ((size jint)) :returning :pointer-void)
(ffi:def-function "java_new_long_array" ((size jint)) :returning :pointer-void)
(ffi:def-function "java_new_float_array" ((size jint)) :returning :pointer-void)
(ffi:def-function "java_new_double_array" ((size jint)) :returning :pointer-void)

(ffi:def-function "java_get_object_array_elt" ((array :pointer-void) (index jint)) :returning :pointer-void)
(ffi:def-function "java_get_boolean_array_elt" ((array :pointer-void) (index jint)) :returning jboolean)
(ffi:def-function "java_get_byte_array_elt" ((array :pointer-void) (index jint)) :returning jbyte)
(ffi:def-function "java_get_char_array_elt" ((array :pointer-void) (index jint)) :returning jchar)
(ffi:def-function "java_get_short_array_elt" ((array :pointer-void) (index jint)) :returning jshort)
(ffi:def-function "java_get_int_array_elt" ((array :pointer-void) (index jint)) :returning jint)
(ffi:def-function "java_get_long_array_elt" ((array :pointer-void) (index jint)) :returning jlong)
(ffi:def-function "java_get_float_array_elt" ((array :pointer-void) (index jint)) :returning jfloat)
(ffi:def-function "java_get_double_array_elt" ((array :pointer-void) (index jint)) :returning jdouble)

(ffi:def-function "java_get_boolean_array_elements" ((array :pointer-void)) :returning (* 'jboolean))
(ffi:def-function "java_get_byte_array_elements" ((array :pointer-void)) :returning (* 'jbyte))
(ffi:def-function "java_get_char_array_elements" ((array :pointer-void)) :returning (* 'jchar))
(ffi:def-function "java_get_short_array_elements" ((array :pointer-void)) :returning (* 'jshort))
(ffi:def-function "java_get_int_array_elements" ((array :pointer-void)) :returning (* 'jint))
(ffi:def-function "java_get_long_array_elements" ((array :pointer-void)) :returning (* 'jlong))
(ffi:def-function "java_get_float_array_elements" ((array :pointer-void)) :returning (* 'jfloat))
(ffi:def-function "java_get_double_array_elements" ((array :pointer-void)) :returning (* 'jdouble))

(ffi:def-function "java_release_boolean_array_elements" ((array :pointer-void) (carray (* jboolean))) :returning :void)
(ffi:def-function "java_release_byte_array_elements" ((array :pointer-void) (carray (* jbyte))) :returning :void)
(ffi:def-function "java_release_char_array_elements" ((array :pointer-void) (carray (* jchar))) :returning :void)
(ffi:def-function "java_release_short_array_elements" ((array :pointer-void) (carray (* jshort))) :returning :void)
(ffi:def-function "java_release_int_array_elements" ((array :pointer-void) (carray (* jint))) :returning :void)
(ffi:def-function "java_release_long_array_elements" ((array :pointer-void) (carray (* jlong))) :returning :void)
(ffi:def-function "java_release_float_array_elements" ((array :pointer-void) (carray (* jfloat))) :returning :void)
(ffi:def-function "java_release_double_array_elements" ((array :pointer-void) (carray (* jdouble))) :returning :void)

(ffi:def-function "java_fill_object_array_region" ((array :pointer-void) (index jint) (len jint) (x :pointer-void)) :returning :void)
(ffi:def-function "java_fill_boolean_array_region" ((array :pointer-void) (index jint) (len jint) (x jboolean)) :returning :void)
(ffi:def-function "java_fill_byte_array_region" ((array :pointer-void) (index jint) (len jint) (x jbyte)) :returning :void)
(ffi:def-function "java_fill_char_array_region" ((array :pointer-void) (index jint) (len jint) (x jchar)) :returning :void)
(ffi:def-function "java_fill_short_array_region" ((array :pointer-void) (index jint) (len jint) (x jshort)) :returning :void)
(ffi:def-function "java_fill_int_array_region" ((array :pointer-void) (index jint) (len jint) (x jint)) :returning :void)
(ffi:def-function "java_fill_long_array_region" ((array :pointer-void) (index jint) (len jint) (x jlong)) :returning :void)
(ffi:def-function "java_fill_float_array_region" ((array :pointer-void) (index jint) (len jint) (x jfloat)) :returning :void)
(ffi:def-function "java_fill_double_array_region" ((array :pointer-void) (index jint) (len jint) (x jdouble)) :returning :void)

(ffi:def-function "java_string_length" ((string :pointer-void)) :returning jint)
(ffi:def-function "java_string_char_array" ((string :pointer-void)) :returning :pointer-void)
(ffi:def-function "java_string_release_char_array" ((string :pointer-void) (array (* jchar))) :returning :void)
(ffi:def-function "java_new_string" ((array (* jchar)) (length jint)) :returning :pointer-void)

(ffi:def-function "java_instanceof" ((obj :pointer-void) (class :pointer-void)) :returning jboolean)

;; ***** Helper functions for macros *****
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun jni-array-type (type)
    (case type
      (:boolean 'jboolean-array)
      (:byte 'jbyte-array)
      (:char 'jchar-array)
      (:short 'jshort-array)
      (:int 'jint-array)
      (:long 'jlong-array)
      (:float 'jfloat-array)
      (:double 'jdouble-array)
      (t :pointer-void)))
    
  (defun type->string (type)
    (if (listp type)
	(if (and (equal (symbol-name (first type)) "[")
		 (= (length type) 2))
	    (concatenate 'string "[" (type->string (second type)))
	    (error "Wrong type declaration!"))
	(case type
	  (:boolean "Z")
	  (:byte "B")
	  (:char "C")
	  (:short "S")
	  (:int "I")
	  (:long "J")
	  (:float "F")
	  (:double "D")
	  (:void "V")
	  (t (concatenate 'string "L" type ";")))))

  (defun type->class-descriptor (type)
    (if (listp type)
	(if (and (equal (symbol-name (first type)) "[")
		 (= (length type) 2))
	    (concatenate 'string "[" (type->string (second type)))
	    (error "Wrong type declaration!"))
	type))

  (defun signature (args returntype)
    (concatenate 'string "("
		 (reduce #'(lambda (&optional x y) (concatenate 'string x y))
			  (mapcar #'(lambda (x) (type->string (second x))) args))
		  ")" (type->string returntype)))

  (defun set-jvalue (jvalues pos x slot)
    (setf (ffi:get-slot-value (jvalue-array-pointer jvalues pos) 'jvalue slot) x))
  ;; Wrapper functions for conversion of lisp types to jvalue types
  (defun java-boolean (jvalues pos x)
    (if (eql x nil)
	(set-jvalue jvalues pos 0 'z)
	(set-jvalue jvalues pos 1 'z)))
  (defun java-byte (jvalues pos x)
    (if (integerp x)
	(if (<= (integer-length x) 8)
	    (set-jvalue jvalues pos x 'b)
	    (error 'type-error :datum x :expected-type :byte))
	(error 'type-error :datum x :expected-type :byte)))
  (defun java-char (jvalues pos x)
    (if (and (integerp x) (>= x 0))
	(if (<= (integer-length x) 16)
	    (set-jvalue jvalues pos x 'c)
	    (error 'type-error :datum x :expected-type :char))
	(error 'type-error :datum x :expected-type :char)))
  (defun java-short (jvalues pos x)
    (if (integerp x)
	(if (<= (integer-length x) 16)
	    (set-jvalue jvalues pos x 's)
	    (error 'type-error :datum x :expected-type :short))
	(error 'type-error :datum x :expected-type :short)))
  (defun java-int (jvalues pos x)
    (if (integerp x)
	(if (<= (integer-length x) 32)
	    (set-jvalue jvalues pos x 'i)
	    (error 'type-error :datum x :expected-type :int))
	(error 'type-error :datum x :expected-type :int)))
  (defun java-long (jvalues pos x)
    (if (integerp x)
	(if (<= (integer-length x) 64)
	    (set-jvalue jvalues pos x 'j)
	    (error 'type-error :datum x :expected-type :long))
	(error 'type-error :datum x :expected-type :long)))
  (defun java-float (jvalues pos x)
    (if (realp x)
	(set-jvalue jvalues pos (coerce x 'single-float) 'f)
	(error 'type-error :datum x :expected-type :float)))
  (defun java-double (jvalues pos x)
    (if (realp x)
	(set-jvalue jvalues pos (coerce x 'double-float) 'd)
	(error 'type-error :datum x :expected-type :double)))
  (defun java-pointer (jvalues pos x)
    (set-jvalue jvalues pos x 'l))
  
  (defun type-wrapper (type)
    (case type
      (:boolean 'java-boolean)
      (:char 'java-char)
      (:byte 'java-byte)
      (:short 'java-short)
      (:int 'java-int)
      (:long 'java-long)
      (:float 'java-float)
      (:double 'java-double)
      (t 'java-pointer)))
  ;; Wraps every element of the argument list args in a type-wrapper
  ;; e.g. ((a :int) (b :char)) -> ((java-int a) (java-char b))
  (defun args-conversion (args jvalue-array-sym)
    (loop for i below (length args) collecting
	 `(,(type-wrapper (second (nth i args))) ,jvalue-array-sym ,i ,(first (nth i args)))))

  (defun simple-java-boolean (x)
    (if (eql x nil) 0 1))
  (defun simple-java-byte (x)
    (if (integerp x)
	(if (<= (integer-length x) 8)
	    x
	    (error 'type-error :datum x :expected-type :byte))
	(error 'type-error :datum x :expected-type :byte)))
  (defun simple-java-char (x)
    (if (and (integerp x) (>= x 0))
	(if (<= (integer-length x) 16)
	    x
	    (error 'type-error :datum x :expected-type :char))
	(error 'type-error :datum x :expected-type :char)))
  (defun simple-java-short (x)
    (if (integerp x)
	(if (<= (integer-length x) 16)
	    x
	    (error 'type-error :datum x :expected-type :short))
	(error 'type-error :datum x :expected-type :short)))
  (defun simple-java-int (x)
    (if (integerp x)
	(if (<= (integer-length x) 32)
	    x
	    (error 'type-error :datum x :expected-type :int))
	(error 'type-error :datum x :expected-type :int)))
  (defun simple-java-long (x)
    (if (integerp x)
	(if (<= (integer-length x) 64)
	    x
	    (error 'type-error :datum x :expected-type :long))
	(error 'type-error :datum x :expected-type :long)))
  (defun simple-java-float (x)
    (if (realp x)
	(coerce x 'single-float)
	(error 'type-error :datum x :expected-type :float)))
  (defun simple-java-double (x)
    (if (realp x)
	(coerce x 'double-float)
	(error 'type-error :datum x :expected-type :double)))
  (defun simple-java-pointer (x)
    x)
  ;; Simple type wrappers do not save the argument in a jvalue, but instead
  ;; directly return it
  (defun simple-type-wrapper (type)
    (case type
      (:boolean 'simple-java-boolean)
      (:char 'simple-java-char)
      (:byte 'simple-java-byte)
      (:short 'simple-java-short)
      (:int 'simple-java-int)
      (:long 'simple-java-long)
      (:float 'simple-java-float)
      (:double 'simple-java-double)
      (t 'simple-java-pointer)))

  ;; Booleans need special treatment when returned from java
  (defun java-boolean-method-wrapper (obj methodID args)
    (if (= 0 (java-boolean-method obj methodID args))
	nil
	t))
  (defun java-static-boolean-method-wrapper (class methodID args)
    (if (= 0 (java-static-boolean-method class methodID args))
	nil
	t))
  (defun java-boolean-field-wrapper (obj fieldID)
    (if (= 0 (java-boolean-field obj fieldID))
	nil
	t))
  (defun java-static-boolean-field-wrapper (class fieldID)
    (if (= 0 (java-static-boolean-field class fieldID))
	nil
	t))
  (defun java-get-boolean-array-elt-wrapper (array index)
    (if (= 0 (java-get-boolean-array-elt array index))
	nil
	t))

  ;; The following methods specify which JNI wrappers to call for a given
  ;; return type and action
  (defun java-method (returntype)
    (case returntype
      (:void 'java-void-method)
      (:boolean 'java-boolean-method-wrapper)
      (:byte 'java-byte-method)
      (:char 'java-char-method)
      (:short 'java-short-method)
      (:int 'java-int-method)
      (:long 'java-long-method)
      (:float 'java-float-method)
      (:double 'java-double-method)
      (t 'java-object-method)))

  (defun java-static-method (returntype)
    (case returntype
      (:void 'java-static-void-method)
      (:boolean 'java-static-boolean-method-wrapper)
      (:byte 'java-static-byte-method)
      (:char 'java-static-char-method)
      (:short 'java-static-short-method)
      (:int 'java-static-int-method)
      (:long 'java-static-long-method)
      (:float 'java-static-float-method)
      (:double 'java-static-double-method)
      (t 'java-static-object-method)))

  (defun java-field (type)
    (case type
      (:boolean 'java-boolean-field-wrapper)
      (:byte 'java-byte-field)
      (:char 'java-char-field)
      (:short 'java-short-field)
      (:int 'java-int-field)
      (:long 'java-long-field)
      (:float 'java-float-field)
      (:double 'java-double-field)
      (t 'java-object-field)))
  
  (defun java-static-field (type)
    (case type
      (:boolean 'java-static-boolean-field-wrapper)
      (:byte 'java-static-byte-field)
      (:char 'java-static-char-field)
      (:short 'java-static-short-field)
      (:int 'java-static-int-field)
      (:long 'java-static-long-field)
      (:float 'java-static-float-field)
      (:double 'java-static-double-field)
      (t 'java-static-object-field)))

  (defun java-setf-field (type)
    (case type
      (:boolean 'java-set-boolean-field)
      (:byte 'java-set-byte-field)
      (:char 'java-set-char-field)
      (:short 'java-set-short-field)
      (:int 'java-set-int-field)
      (:long 'java-set-long-field)
      (:float 'java-set-float-field)
      (:double 'java-set-double-field)
      (t 'java-set-object-field)))

  (defun java-setf-static-field (type)
    (case type
      (:boolean 'java-set-static-boolean-field)
      (:byte 'java-set-static-byte-field)
      (:char 'java-set-static-char-field)
      (:short 'java-set-static-short-field)
      (:int 'java-set-static-int-field)
      (:long 'java-set-static-long-field)
      (:float 'java-set-static-float-field)
      (:double 'java-set-static-double-field)
      (t 'java-set-static-object-field)))

  (defun java-new-array (type)
    (case type
      (:boolean 'java-new-boolean-array)
      (:byte 'java-new-byte-array)
      (:char 'java-new-char-array)
      (:short 'java-new-short-array)
      (:int 'java-new-int-array)
      (:long 'java-new-long-array)
      (:float 'java-new-float-array)
      (:double 'java-new-double-array)
      (t 'java-new-object-array)))

  (defun java-array-elt (type)
    (case type
      (:boolean 'java-get-boolean-array-elt-wrapper)
      (:byte 'java-get-byte-array-elt)
      (:char 'java-get-char-array-elt)
      (:short 'java-get-short-array-elt)
      (:int 'java-get-int-array-elt)
      (:long 'java-get-long-array-elt)
      (:float 'java-get-float-array-elt)
      (:double 'java-get-double-array-elt)
      (t 'java-get-object-array-elt)))

  (defun java-fill-array-region (type)
    (case type
      (:boolean 'java-fill-boolean-array-region)
      (:byte 'java-fill-byte-array-region)
      (:char 'java-fill-char-array-region)
      (:short 'java-fill-short-array-region)
      (:int 'java-fill-int-array-region)
      (:long 'java-fill-long-array-region)
      (:float 'java-fill-float-array-region)
      (:double 'java-fill-double-array-region)
      (t 'java-fill-object-array-region)))

  (defun java-array-elements (type)
    (case type
      (:boolean 'java-get-boolean-array-elements)
      (:byte 'java-get-byte-array-elements)
      (:char 'java-get-char-array-elements)
      (:short 'java-get-short-array-elements)
      (:int 'java-get-int-array-elements)
      (:long 'java-get-long-array-elements)
      (:float 'java-get-float-array-elements)
      (:double 'java-get-double-array-elements)
      (t (error "Java-array-elements can be obtained only for primitive java types"))))

  (defun java-release-array-elements (type)
    (case type
      (:boolean 'java-release-boolean-array-elements)
      (:byte 'java-release-byte-array-elements)
      (:char 'java-release-char-array-elements)
      (:short 'java-release-short-array-elements)
      (:int 'java-release-int-array-elements)
      (:long 'java-release-long-array-elements)
      (:float 'java-release-float-array-elements)
      (:double 'java-release-double-array-elements)
      (t (error "Java-array-elements can be obtained only for primitive java types"))))
      
  ;; A java-class object holds the cached jclass, jmethodID and jfieldID
  ;; attributes
  (defclass java-class ()
    ((name :initarg :name)
     (jclass-object :initform NIL)
     (methodIDs :initarg :methodIDs)
     (fieldIDs :initarg :fieldIDs)
     (constructorIDs :initarg :constructorIDs)))

  ;; Needed for caching of jclass object of LispCondition class
  (defvar *LispCondition* (make-instance 'java-class
	       :name "jffi/LispCondition"))
  ;; Tests whether an exception has occurred during the call, which
  ;; returned object. If yes, throws the Lisp-condition
  ;; 'jffi:java-exception with the corresponding exception
  (defun safe-return (object)
    (let ((exception (java-test-exception)))
      (if (ffi:null-pointer-p exception)
	  object
	  (if (= (java-instanceof exception (get-jclass *LispCondition*)) 0)
	      (error 'java-exception :exception exception)
	      (signal-lisp-condition exception)))))
  ;; As above, but checks for object == NULL before looking for an exception
  (defun safe-return-null-pointer (object)
    (if (ffi:null-pointer-p object)
	(safe-return object)
	object))

  ;; The following functions extract (and automatically reload) the cached
  ;; attributes in a java-class object
  (defun get-jclass (csym)
    (if (eql nil (slot-value csym 'jclass-object))
	(setf (slot-value csym 'jclass-object) (add-global-reference (safe-return-null-pointer (java-jclass (slot-value csym 'name)))))
	(slot-value csym 'jclass-object)))

  (defun get-constructorID (pos csym signature)
    `(if (eql nil (elt (slot-value ,csym 'constructorIDs) ,pos))
	 (setf (elt (slot-value ,csym 'constructorIDs) ,pos)
	       (safe-return-null-pointer (java-constructorID (get-jclass ,csym) ,signature)))
	 (elt (slot-value ,csym 'constructorIDs) ,pos)))

  (defun IDmethod-name (IDname allocation)
    (case IDname
      ('methodIDs
       (if (eql allocation :static)
	   'java-static-jmethodID
	   'java-jmethodID))
      ('fieldIDs
       (if (eql allocation :static)
	   'java-static-jfieldID
	   'java-jfieldID))
      (t (error (format nil "The ID ~a is unknown!" IDname)))))
  (defun get-ID (IDname pos csym name signature &key (allocation :instance))
    `(if (eql nil (elt (slot-value ,csym (quote ,IDname)) ,pos))
	 (setf (elt (slot-value ,csym (quote ,IDname)) ,pos)
	       (safe-return-null-pointer
		(,(IDmethod-name IDname allocation)
		  (get-jclass ,csym)
		  ,name ,signature)))
	 (elt (slot-value ,csym (quote ,IDname)) ,pos)))

  ;; Convenience method for building compound symbol names
  (defun symbol-name-with-prefix (prefix name)
    (string-upcase
     (nsubstitute #\- #\_
		  (if (eql nil prefix)
		      name
		      (concatenate 'string prefix "." name)))))
  ;; Does not save the returned global reference in the
  ;; *java-global-references* array as add-global-reference does
  (defun simple-add-global-reference (obj)
    (safe-return-null-pointer (java-new-global-ref obj)))
)

;; a finalizer-wrapper is wrapped around functions, which return
;; non-primitive java objects
(defmacro finalizer-wrapper (finalizer type body)
  (if (or (stringp type) (and (symbolp type) (not (keywordp type))))
      (if (eql finalizer :local)
	  body
	  (if finalizer
	      (let ((jobj (gensym)))
		`(let ((,jobj (simple-add-global-reference ,body)))
		   (ext:set-finalizer ,jobj
				      #'java-delete-global-ref)
		   ,jobj))
	      `(simple-add-global-reference ,body)))
      body))

;; ***** Macros to define lisp functions, which invoke java methods or
;;       set and get fields of java classes *****
;; Keys: - prefix: Names of lisp-functions corresponding to java-methods,
;;          fields or constructors are of the form (prefix.lisp-name) or
;;          (lisp-name) if prefix is nil. Defaults to classname. Must be
;;          of type string
;;       - class-symbol: symbol with the name of the global object holding
;;          cached JNI structures. Defaults to *prefix* or if prefix is nil
;;          to *classname*.
(defmacro def-class (package classname &key (prefix classname) (class-symbol nil) (fields nil) (methods nil) (constructors nil))
  "def-class is used to define a Java-class with its methods, fields and
constructors you want to use. For each method, field or constructor, a
Lisp-function is defined, which lets you call the corresponding Java-
method or access the field of the Java-class. Additionally a global
object with the name class-symbol is created, which holds cached JNI
values."
  (let ((csym (if (eql class-symbol nil)
		  (intern (string-upcase
			   (nsubstitute #\- #\_ (concatenate 'string "*"
							     (if (eql nil prefix)
								 classname
								 prefix)
							     "*")))
			  *package*)
		  class-symbol)))
    `(progn
       (defvar ,csym
	 (make-instance 'java-class
			:name ,(ffi:convert-to-cstring
				(if (equal package "")
				    classname
				    (concatenate 'string package "/" classname)))
			:methodIDs ,(make-array (length methods) :initial-element NIL)
			:fieldIDs ,(make-array (length fields) :initial-element NIL)
			:constructorIDs ,(make-array (length constructors) :initial-element NIL)))
       ,@(loop for i below (length fields) collect
	      (let ((fdef (elt fields i)))
		(if (listp (first fdef))
		    `(def-field ,csym ,i
		       ,(intern (symbol-name-with-prefix prefix
							 (symbol-name (second (first fdef))))
				*package*)
		       ,(first (first fdef)) ,@(rest fdef))
		    `(def-field ,csym ,i
		       ,(intern (symbol-name-with-prefix prefix
							 (first fdef))
				*package*)
		       ,@fdef))))
       ,@(loop for i below (length fields) collect
	      (let ((fdef (elt fields i)))
		(if (listp (first fdef))
		    `(def-setf-field ,csym ,i
		       ,(intern (symbol-name-with-prefix prefix
							 (symbol-name (second (first fdef))))
				*package*)
		       ,(first (first fdef)) ,@(rest fdef))
		    `(def-setf-field ,csym ,i
		       ,(intern (symbol-name-with-prefix prefix
							 (first fdef))
				*package*)
		       ,@fdef))))
       ,@(loop for i below (length methods) collect
	      (let ((mdef (elt methods i)))
		(if (listp (first mdef))
		    `(def-method ,csym ,i
		       ,(intern (symbol-name-with-prefix prefix
							 (symbol-name (second (first mdef))))
				*package*)
		       ,(first (first mdef)) ,@(rest mdef))
		    `(def-method ,csym ,i
		       ,(intern (symbol-name-with-prefix prefix
							 (first mdef))
				*package*)
		       ,@mdef))))
       ,@(loop for i below (length constructors) collect
	      `(def-constructor ,csym ,i
		 ,(intern (symbol-name-with-prefix prefix
						   (symbol-name (first (elt constructors i))))
			  *package*)
		 ,@(rest (elt constructors i)))))))

(defmacro def-method (csym pos name java-name args &key (returning :void) (allocation :instance) (finalizer t))
  (case allocation
    (:instance `(def-instance-method ,csym ,pos ,name ,java-name ,args :returning ,returning :finalizer ,finalizer))
    (:static `(def-static-method ,csym ,pos ,name ,java-name ,args :returning ,returning :finalizer ,finalizer))
    (t (error "Unkown allocation: Valid values are :static or :instance"))))

(defmacro def-field (csym pos name java-name &key type (allocation :instance) (finalizer t))
  (case allocation
    (:instance `(def-instance-field ,csym ,pos ,name ,java-name :type ,type :finalizer ,finalizer))
    (:static `(def-static-field ,csym ,pos ,name ,java-name :type ,type :finalizer ,finalizer))
    (t (error "Unkown allocation: Valid values are :static or :instance"))))

(defmacro def-setf-field (csym pos name java-name &key type (allocation :instance) (finalizer t))
  (case allocation
    (:instance `(def-instance-setf-field ,csym ,pos ,name ,java-name :type ,type))
    (:static `(def-static-setf-field ,csym ,pos ,name ,java-name :type ,type))
    (t (error "Unkown allocation: Valid values are :static or :instance"))))

(defmacro def-constructor (csym pos name args &key (finalizer t))
  (let* ((jvalue-array-sym (gensym))
	 (ret-value (gensym))
	 (signature (signature args :void))
	 (cargs (args-conversion args jvalue-array-sym))
	 (largs (mapcar #'first args)))
    (if (eql args nil)
	`(defun ,name ,largs
	   (finalizer-wrapper
	     ,finalizer "" ;; Empty string just leads to an object wrapper
	     (safe-return-null-pointer
	       (java-constructor
		(get-jclass ,csym)
		,(get-constructorID pos csym signature)
		(ffi:make-null-pointer 'jvalue)))))
	`(defun ,name ,largs
	   (let ((,jvalue-array-sym (ffi:allocate-foreign-object 'jvalue ,(length args))))
	     ,@cargs
	     (let ((,ret-value
		    (finalizer-wrapper
		      ,finalizer ""
		      (safe-return-null-pointer
			(java-constructor
			 (get-jclass ,csym)
			 ,(get-constructorID pos csym signature)
			 ,jvalue-array-sym)))))
		   (ffi:free-foreign-object ,jvalue-array-sym)
		   ,ret-value))))))

(defmacro def-instance-method (csym pos name java-name args &key (returning :void) (finalizer t))
  (let* ((jobject-sym (gensym))
	 (jvalue-array-sym (gensym))
	 (ret-value (gensym))
	 (signature (signature args returning))
	 (cargs (args-conversion args jvalue-array-sym))
	 (largs (cons jobject-sym (mapcar #'first args)))
	 (method (java-method returning)))
    (if (eql args nil)
	`(defun ,name ,largs
	   (finalizer-wrapper
	    ,finalizer ,returning
	    (safe-return
	     (,method
	      ,jobject-sym
	      ,(get-ID 'methodIDs pos csym java-name signature :allocation :instance)
	      (ffi:make-null-pointer 'jvalue)))))
	`(defun ,name ,largs
	   (let ((,jvalue-array-sym (ffi:allocate-foreign-object 'jvalue ,(length args))))
	     ,@cargs
	     (let ((,ret-value
		    (finalizer-wrapper
		     ,finalizer ,returning
		     (safe-return
		      (,method
		       ,jobject-sym
		       ,(get-ID 'methodIDs pos csym java-name signature :allocation :instance)
		       ,jvalue-array-sym)))))
	       (ffi:free-foreign-object ,jvalue-array-sym)
	       ,ret-value))))))

(defmacro def-static-method (csym pos name java-name args &key (returning :void) (finalizer t))
  (let* ((jvalue-array-sym (gensym))
	 (ret-value (gensym))
	 (signature (signature args returning))
	 (cargs (args-conversion args jvalue-array-sym))
	 (largs (mapcar #'first args))
	 (method (java-static-method returning)))
    (if (eql args nil)
	`(defun ,name ,largs
	   (finalizer-wrapper
	     ,finalizer ,returning
	     (safe-return
	      (,method
	       (get-jclass ,csym)
	       ,(get-ID 'methodIDs pos csym java-name signature :allocation :static)
	       (ffi:make-null-pointer 'jvalue)))))
	`(defun ,name ,largs
	   (let ((,jvalue-array-sym (ffi:allocate-foreign-object 'jvalue ,(length args))))
	     ,@cargs
	     (let ((,ret-value
		    (finalizer-wrapper
		     ,finalizer ,returning
		     (safe-return
		      (,method
		       (get-jclass ,csym)
		       ,(get-ID 'methodIDs pos csym java-name signature :allocation :static)
		       ,jvalue-array-sym)))))
	       (ffi:free-foreign-object ,jvalue-array-sym)
	       ,ret-value))))))

(defmacro def-instance-field (csym pos name java-name &key type (finalizer t))
  (let ((jobject-sym (gensym))
	(signature (type->string type))
	(field-method (java-field type)))
    `(defun ,name (,jobject-sym)
       (finalizer-wrapper
	,finalizer ,type
	(safe-return
	 (,field-method
	  ,jobject-sym
	  ,(get-ID 'fieldIDs pos csym java-name signature :allocation :instance)))))))

(defmacro def-static-field (csym pos name java-name &key type (finalizer t))
  (let ((signature (type->string type))
	(field-method (java-static-field type)))
    `(defun ,name ()
       (finalizer-wrapper
	,finalizer ,type
	(safe-return
	 (,field-method
	  (get-jclass ,csym)
	  ,(get-ID 'fieldIDs pos csym java-name signature :allocation :static)))))))

(defmacro def-instance-setf-field (csym pos name java-name &key type)
  (let ((setf-arg-sym (gensym))
	(jobject-sym (gensym))
	(signature (type->string type))
	(field-method (java-setf-field type))
	(wrapper (simple-type-wrapper type)))
    `(defsetf ,name (,jobject-sym) (,setf-arg-sym)
       `(safe-return
	 (,(quote ,field-method)
	   ,,jobject-sym
	   ,(quote ,(get-ID 'fieldIDs pos csym java-name signature :allocation :instance))
	   (,(quote ,wrapper) ,,setf-arg-sym))))))

(defmacro def-static-setf-field (csym pos name java-name &key type)
  (let ((setf-arg-sym (gensym))
	(signature (type->string type))
	(field-method (java-setf-static-field type))
	(wrapper (simple-type-wrapper type)))
    `(defsetf ,name () (,setf-arg-sym)
       `(safe-return
	 (,(quote ,field-method)
	   ,(quote (get-jclass ,csym))
	   ,(quote ,(get-ID 'fieldIDs pos csym java-name signature :allocation :static))
	   (,(quote ,wrapper) ,,setf-arg-sym))))))

;; ***** Java-Arrays *****
(defmacro new-array (type size &key (initial-element
					  (case type
					    ((:byte :char :short :int :long :float :double) 0)
					    (t nil)))
				 (finalizer t))
  "Creates a new Java-array of with the specified type, size and initial-element"
  (if (and (symbolp type) (not (keywordp type)))
      `(finalizer-wrapper
	,finalizer ""
	(safe-return-null-pointer
	 (,(java-new-array type) ,size (get-jclass ,type)
	   ,(if (eql initial-element nil)
		'(ffi:make-null-pointer :pointer-void)
		initial-element))))
      (if (not (keywordp type))
	  `(finalizer-wrapper
	    ,finalizer ""
	    (safe-return-null-pointer
	     (,(java-new-array type) ,size
	       (safe-return-null-pointer (java-jclass
					  ,(type->class-descriptor type)))
	       ,(if (eql initial-element nil)
		    '(ffi:make-null-pointer :pointer-void)
		    initial-element))))
	  (let ((array-name (gensym)))
	    `(let ((,array-name
		    (finalizer-wrapper
		     ,finalizer ""
		     (safe-return-null-pointer
		      (,(java-new-array type) ,size)))))
	       (fill-array ,array-name ,type ,initial-element))))))

(defmacro array-elt (array type index &key (finalizer t))
  "Returns the element at the specified index of the array. This is a setf-able place."
  `(finalizer-wrapper
    ,finalizer ,type
    (safe-return (,(java-array-elt type) ,array ,index))))
(defmacro set-array-elt (array type index item)
  `(progn (safe-return (,(java-fill-array-region type) ,array ,index 1 (,(simple-type-wrapper type) ,item)))
	  ,item))
(defsetf array-elt set-array-elt)
(defmacro fill-array (array type item &key (start 0) (end nil))
  "Fills the region specified by start and end of the array of with the given item"
  `(progn (safe-return (,(java-fill-array-region type) ,array ,start
			 ,(if (eql 0 start)
			      (if (eql nil end)
				`(array-length ,array)
				end)
			      `(- ,(if (eql nil end)
				       `(array-length ,array)
				       end)
				  ,start))
			 (,(simple-type-wrapper type) ,item)))
	  ,array))

;; ***** Array-conversion *****
(defmacro convert-to-jarray (array type &key (finalizer t))
  "Converts the given Lisp-array to a Java-array"
  (if (not (keywordp type))
      (let ((eval-array (gensym))
	    (narray (gensym))
	    (length (gensym))
	    (i (gensym)))
	`(let* ((,eval-array (eval ,array))
		(,length (length ,eval-array))
		(,narray (new-array ,type ,length :finalizer ,finalizer)))
	   (loop for ,i below ,length
	      do (setf (array-elt ,narray ,type ,i) (elt ,eval-array ,i)))
	   ,narray))
      (let ((eval-array (gensym))
	    (narray (gensym))
	    (ncarray (gensym))
	    (length (gensym))
	    (i (gensym))
	    (jni-array-type (jni-array-type type)))
	`(let* ((,eval-array (eval ,array))
		(,length (length ,eval-array))
		(,narray (new-array ,type ,length :finalizer ,finalizer))
		(,ncarray (,(java-array-elements type) ,narray)))
	   (loop for ,i below ,length
	      do (setf (ffi:deref-array ,ncarray (quote ,jni-array-type) ,i)
		       (,(simple-type-wrapper type) (elt ,eval-array ,i))))
	   (,(java-release-array-elements type) ,narray ,ncarray)
	   ,narray))))

(defmacro convert-from-jarray (array type &key (finalizer t) (element-type t) (adjustable nil) (fill-pointer nil))
  "Converts the given Java-array to a Lisp-array"
  (if (not (keywordp type))
      (let ((eval-array (gensym))
	    (narray (gensym))
	    (length (gensym))
	    (i (gensym))
	    (element (gensym)))
	`(let* ((,eval-array (eval ,array))
		(,length (array-length ,eval-array))
		(,narray (make-array `(,,length)
				       :element-type ,element-type
				       :adjustable ,adjustable
				       :fill-pointer ,fill-pointer)))
	   (loop for ,i below ,length
	      do (setf (elt ,narray ,i)
		       (array-elt ,eval-array ,type ,i :finalizer ,finalizer)))
	   ,narray))
      (let ((eval-array (gensym))
	    (narray (gensym))
	    (length (gensym))
	    (i (gensym))
	    (carray (gensym))
	    (jni-array-type (jni-array-type type)))
	`(let* ((,eval-array (eval ,array))
		(,length (array-length ,eval-array))
		(,narray (make-array `(,,length)
				       :element-type ,element-type
				       :adjustable ,adjustable
				       :fill-pointer ,fill-pointer))
		(,carray (,(java-array-elements type) ,eval-array)))
	   (loop for ,i below ,length
	      do (setf (elt ,narray ,i)
		       ,(if (eql type :boolean)
			    `(if (= 0 (ffi:deref-array
				       ,carray
				       (quote ,jni-array-type)
				       ,i))
				 nil t)
			    `(ffi:deref-array
			      ,carray
			      (quote ,jni-array-type)
			      ,i))))
	   (,(java-release-array-elements type) ,eval-array ,carray)
	 ,narray))))

;; ***** String conversion *****
(defun utf16->utf32 (h l)
  (+ (ash (- h #xD800) 10) (- l #xDC00) #x10000))
(defun utf32->utf16-high (c)
  (+ (ash (- c #x10000) -10) #xD800))
(defun utf32->utf16-low (c)
  (+ (logand (- c #x10000) #x3FF) #xDC00))
;; returns nil, if jstring is null
(defun convert-from-jstring (jstring)
  "Converts the given Java-string to a Lisp-string"
  (if (ffi:null-pointer-p jstring)
      nil
      (let* ((length (safe-return (java-string-length jstring)))
	     (char-array (safe-return (java-string-char-array jstring)))
	     (base-only (not (loop for i below length if
				  (>= (ffi:deref-array char-array 'jchar-array i) 256)
				return t))))
	(if base-only
	    (let ((ret (make-string length :element-type 'base-char)))
	      (loop for i below length
		 do (setf (elt ret i) (code-char (ffi:deref-array char-array 'jchar-array i))))
	      (java-string-release-char-array jstring char-array)
	      ret)
	    (let* ((utf32-length (- length (loop for i below length count
						(and (>= (ffi:deref-array char-array 'jchar-array i) #xD800)
						     (<= (ffi:deref-array char-array 'jchar-array i) #xDBFF)))))
		   (ret (make-string utf32-length :element-type 'extended-char)))
	      (loop for i below length
		 for j below utf32-length
		 do (let ((utf16-code (ffi:deref-array char-array 'jchar-array i)))
		      (if (and (>= utf16-code #xD800)
			       (<= utf16-code #xDBFF)
			       (< (1+ i) length))
			  (progn (setf (elt ret j) (code-char
						    (utf16->utf32 utf16-code
								  (ffi:deref-array char-array 'jchar-array (1+ i)))))
				 (setf i (1+ i)))
			  (setf (elt ret j) (code-char utf16-code)))))
	      (java-string-release-char-array jstring char-array)
	      ret)))))

(defun convert-to-jstring (string &key (finalizer t))
  "Converts the given Lisp-string to a Java-string"
  (if (stringp string)
      (let* ((jlength (+ (length string)
			 (loop for i across string
			    count (>= (char-code i) #x10000))))
	     (char-array (ffi:allocate-foreign-object 'jchar jlength)))
	(loop for i across string
	   for j below jlength
	   do (if (>= (char-code i) #x10000)
		  (progn
		    (setf (ffi:deref-array char-array 'jchar-array j)
			  (utf32->utf16-high (char-code i)))
		    (setf (ffi:deref-array char-array 'jchar-array (1+ j))
			  (utf32->utf16-low (char-code i)))
		    (setf j (1+ j)))
		  (setf (ffi:deref-array char-array 'jchar-array j)
			(char-code i))))
	(if (eql finalizer :local)
	     (safe-return (java-new-string char-array jlength))
	     (let ((jstr (simple-add-global-reference
			  (safe-return
			   (java-new-string char-array jlength)))))
	       (if finalizer
		   (ext:set-finalizer jstr
				      #'java-delete-global-ref))
				      ;; #'global-ref-finalizer))
	       jstr)))
      (error 'type-error :datum string :expected-type 'string)))

;; ***** Instanceof *****
(defmacro instanceof (obj type)
  "Tests whether obj if an instance of the class specified by type"
  (if (and (symbolp type) (not (keywordp type)))
      `(if (= (safe-return (java-instanceof ,obj (get-jclass ,type))) 0)
	   nil t)
      `(if (= (safe-return
	       (java-instanceof ,obj
				(safe-return-null-pointer
				 (java-jclass ,(type->class-descriptor type)))))
	      0)
	   nil t)))

;; ***** Java-Null *****
(defmacro make-null ()
  "Creates a new Java-null"
  `(ffi:make-null-pointer :pointer-void))
(defmacro null-p (obj)
  "Tests whether obj is a Java-null"
  `(ffi:null-pointer-p ,obj))

;; ***** java-eq implements the (== in Java, eq in Lisp) comparision *****
(defun java-eq (object1 object2)
  "Tests whether object1 is the same object as object2"
  (if (= (java-is-same-object object1 object2) 0) nil t))


;; ***** Support for deleting references to java-objects *****
(defun unref (obj)
  "Deletes the global reference pointing to obj"
  (progn (ext:set-finalizer obj nil)
	 (java-delete-global-ref obj)))
(defun unref-local (obj)
  "Deletes the local reference pointing to obj"
  (java-delete-local-ref obj))
;; Same syntax as let*, all variables defined are automically unref'd
;; after execution of the body
(defmacro with-unref (variables &body body)
  "Declares the given variables like in a let* expression, executes body
and then calls unref on them"
  `(let* (,@variables)
     (unwind-protect
  	  (progn ,@body)
       ,@(loop for i in variables collect `(unref ,(first i))))))
(defmacro with-unref-local (variables &body body)
  "Declares the given variables like in a let* expression, executes body
and then calls unref-local on them"
  `(let* (,@variables)
     (unwind-protect
  	  (progn ,@body)
       ,@(loop for i in variables collect `(unref-local ,(first i))))))
;; Useful for returning Java objects without finalizers to the Java
;; environment
(defun as-local-reference (obj)
  "Converts the given global reference to a local one. Finalizers of obj are removed."
  (progn (ext:set-finalizer obj nil)
	 (java-new-local-ref obj)))
(defun as-global-reference (obj)
  "Converts the given global reference to a local one. Finalizers of obj are removed."
  (java-new-global-ref obj))

;; ***** Java-Exceptions *****
(define-condition java-exception (error)
  ((exception :initarg :exception :reader get-java-exception))
  (:documentation "A Lisp-condition holding a Java-exception"))
(def-class "java/lang" "Throwable"
  :methods (("getMessage" () :returning "java/lang/String" :finalizer nil)
	    ("toString" () :returning "java/lang/String" :finalizer nil)))
(defun java-exception-message (exception)
  "Returns a Lisp-string with the output of calling .getMessage() on the
exception"
  (with-unref ((message (Throwable.getMessage
			 (slot-value exception 'exception))))
    (convert-from-jstring message)))
(defun java-exception-to-string (exception)
  "Returns a Lisp-string with the output of calling .toString() on the
exception"
  (with-unref ((ret (Throwable.toString
		     (slot-value exception 'exception))))
    (convert-from-jstring ret)))

;; ***** Error handling for the JNI Invocation API *****
(define-condition java-environment-error (simple-error) ())
(defconstant jni-ok 0)
(defconstant jni-detached-error -2)
(defconstant jni-version-error -3)
;; This errors are not in the JNI specification, but defined in jni.h
;; Hopefully these are the same in all implementations
(defconstant jni-already-exists-error -5)
(defconstant jni-invalid-arguments-error -6)
;; Custom error code, thrown if no java vm has been created, but it is
;; attempted to destroy one
(defconstant jffi-no-jvm-running-error -1000)

;; handler-defs := (jni-code format-control)
;; Each handler-def corresponds to a JNI error code with its format control
;; for the java-environment-error, which should be signaled if
;; (= jni-code error-code)
(defmacro jni-error-handler (error-code default-format-control
			     &rest handler-defs)
  (let ((cond-args `(((= ,error-code jni-ok) t))))
    (loop for i in handler-defs
       do (setf cond-args
		(append cond-args
			`(((= ,error-code ,(first i))
			   (error 'java-environment-error :format-control
				  ,(second i)))))))
    (setf cond-args
	  (append cond-args
		  `((t
		     (error 'java-environment-error :format-control
			    ,default-format-control)))))
    `(cond ,@cond-args)))

;; ***** Multithread support *****
(defun attach-current-thread ()
  "Attaches the current thread to the Java VM"
  (let ((error-code (java-attach-current-thread)))
    (jni-error-handler
     error-code
     (format nil "Can't attach the current thread to the Java VM because of an unknown error with error code ~a" error-code)
     (jni-version-error "Can't attach the current thread to the Java VM because of a JNI version missmatch"))))

(defun detach-current-thread ()
  "Detaches the current thread to the Java VM"
  (let ((error-code (java-detach-current-thread)))
    (jni-error-handler
     error-code
     (format nil "Can't detach the current thread from the Java VM because of an unknown error with error code ~a" error-code)
     (jni-version-error "Can't detach the current thread from the Java VM because of a JNI version missmatch")
     (jni-detached-error "Can't detach the current thread from the Java VM because the calling thread is not attached"))))

;; ***** Create and Destroy a Java VM from Lisp *****
(ffi:def-array-pointer char** :cstring)
(defun create-java-vm (&rest options)
  "Creates a new Java VM with the given options"
  (let* ((nopt (length options))
	 (cstring-options (ffi:allocate-foreign-object :cstring nopt)))
    (loop for i below nopt do
	 (setf (ffi:deref-array cstring-options 'char** i)
	       (ffi:convert-to-cstring (elt options i))))
    (let ((error-code (java-create-java-vm nopt cstring-options)))
      (loop for i below nopt do
	   (ffi:free-cstring (ffi:deref-array cstring-options 'char** i)))
      (ffi:free-foreign-object cstring-options)
      (jni-error-handler
       error-code
       (format nil "Can't create a Java VM because of an unknown error with error code ~a" error-code)
       (jni-already-exists-error "Can't create a Java VM because a VM already exists")
       (jni-invalid-arguments-error "Can't create a Java VM because invalid arguments")
       (jni-version-error "Can't create a Java VM because of a JNI version missmatch")))))
(defun destroy-java-vm ()
  "Destroys the currently running Java VM"
  (let ((error-code (java-destroy-java-vm)))
    (jni-error-handler
     error-code
     (format nil "Can't destroy the running Java VM because of an unknown error with error code ~a" error-code)
     (jffi-no-jvm-running-error "No running Java VM found, which could be destroyed")
     (jni-version-error "Can't destroy the running Java VM because of a JNI version missmatch"))))
(defun is-running ()
  "Returns t if a Java VM is running, nil otherwise"
  (if (= (is-jvm-running) 0) nil t))
  
;; *******************************************************
;; *            Definitions for calls FROM Java          *
;; *******************************************************

;; *java-references* is a vector of references to Lisp objects, which
;; are referenced in Java. This prevents this objects from being garbage
;; collected
(defvar *java-references* (make-array '(0) :adjustable t :fill-pointer t))
(defun remove-reference (obj)
  (progn
    (setq *java-references* (delete obj *java-references* :test #'eq :count 1))))
(defun add-reference (obj)
  (vector-push-extend obj *java-references*))
;; *java-global-references* holds all java objects, which are globally
;; referenced via NewGlobalRef and need to be deleted via DeleteGlobalRef
;; before shutting down the lisp environment
(defvar *java-global-references* (make-array '(0) :adjustable t :fill-pointer t))
(defun add-global-reference (obj)
  (let ((obj-ref (safe-return-null-pointer (java-new-global-ref obj))))
    (progn (vector-push-extend obj-ref *java-global-references*)
	   obj-ref)))
(defun delete-global-reference (obj)
  (progn (java-delete-global-ref obj)
	 (setq *java-global-references*
	       (delete obj *java-global-references* :test #'eq :count 1))))
(defun delete-all-global-references ()
  (progn (loop for i across *java-global-references* do
	      (java-delete-global-ref i))
	 (setq *java-global-references* (make-array '(0) :adjustable t :fill-pointer t))))
