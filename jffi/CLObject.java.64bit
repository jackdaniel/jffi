// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

package jffi;

public class CLObject{
   private long pointer;
   public boolean unrefd = false;
   private CLObject(long p){
      pointer = p;
   }
   protected void finalize() throws Throwable{
      if(!unrefd && Lisp.isRunning()){
	 native_finalize();
	 unrefd = true;
      }
      super.finalize();
   }
   protected native void native_finalize();
   protected native void native_unref();
   public void unref(){
      if(!unrefd){
	 native_unref();
	 unrefd = true;
      }
   }
   public native String toString();
   public native Object toObject();
   public native boolean toBoolean();
   public native byte toByte();
   public native char toChar();
   public native short toShort();
   public native int toInt();
   public native long toLong();
   public native float toFloat();
   public native double toDouble();
}
