// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

#include "jffi_Lisp.h"
#include "jffi_c_layer.h"

extern void init_lib_JFFI_LISP(cl_object);
extern void jffi_global_init();

JNIEXPORT void JNICALL Java_jffi_Lisp_native_1start(JNIEnv *env, jclass mclass){
   if(pseudo_start){
      cl_env_ptr cl_env1 = ecl_process_env();
      ECL_CATCH_ALL_BEGIN(cl_env1) {
      	 set_global_lisp_variables();
      } ECL_CATCH_ALL_IF_CAUGHT {
      	 throw_lisp_environment_error(env, "Error initialising Lisp modules!");
      } ECL_CATCH_ALL_END;
   }
   else{
      char* argv[1];
      argv[0] = "ecl";
      ecl_set_option(ECL_OPT_TRAP_SIGSEGV, FALSE);
      ecl_set_option(ECL_OPT_TRAP_SIGINT, FALSE);
      ecl_set_option(ECL_OPT_TRAP_SIGILL, FALSE);
      ecl_set_option(ECL_OPT_TRAP_SIGBUS, FALSE);
      ecl_set_option(ECL_OPT_TRAP_SIGPIPE, FALSE);
      //ecl_set_option(ECL_OPT_TRAP_SIGFPE, FALSE);
      cl_boot(1, argv);
      //This can't be named cl_env, since this name is already used in the
      //ecl.h include file
      cl_env_ptr cl_env1 = ecl_process_env();
      ECL_CATCH_ALL_BEGIN(cl_env1) {
	 ecl_init_module(NULL, init_lib_JFFI_LISP);
	 jffi_global_init();
	 set_global_lisp_variables();
	 java_env = env;
	 attached_to_lisp = 1;
      } ECL_CATCH_ALL_IF_CAUGHT {
	 throw_lisp_environment_error(env, "Error initialising Lisp modules!");
      } ECL_CATCH_ALL_END;
   }
}
