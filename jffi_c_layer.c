// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include "jffi_Lisp.h"
#include "jffi_CLObject.h"
#include "jffi_c_layer.h"

JavaVM* java_vm = NULL;
__thread JNIEnv* java_env = NULL;
__thread char attached_to_lisp = 0;
char pseudo_start = 0;

void throw_exception(JNIEnv* env){
   jthrowable exception = (*env)->ExceptionOccurred(env);
   (*env)->ExceptionClear(env);
   if(!(*env)->Throw(env, exception)){
      fprintf(stderr, "Can't throw an Exception. Something is very wrong with your Java VM\n");
      exit(-1);
   }
}
void throw_exception_if_occurred(JNIEnv* env){
   jthrowable ret;
   if((ret = (*env)->ExceptionOccurred(env)) != NULL){
      (*env)->ExceptionClear(env);
      (*env)->Throw(env, ret);
   }
}

jclass get_cached_jclass(JNIEnv* env, const char* name, jclass* address){
   if(*address == NULL){
      *address = (*env)->NewGlobalRef(env, (*env)->FindClass(env, name));
      if(*address == NULL)
	 throw_exception(env);
   }
   return *address;
}
jmethodID get_cached_jmethodID(JNIEnv* env, jclass mclass, const char* name, const char* signature, jmethodID* address){
   if(*address == NULL){
      *address = (*env)->GetMethodID(env, mclass, name, signature);
      if(*address == NULL)
	 throw_exception(env);
   }
   return *address;
}
jfieldID get_cached_jfieldID(JNIEnv* env, jclass mclass, const char* name, const char* signature, jfieldID* address){
   if(*address == NULL){
      *address = (*env)->GetFieldID(env, mclass, name, signature);
      if(*address == NULL)
	 throw_exception(env);
   }
   return *address;
}

static jclass clobject_class = NULL;
static jfieldID clobject_pointer = NULL;
static jmethodID clobject_constructor = NULL;
jclass get_clobject_class(JNIEnv* env){
   return get_cached_jclass(env, "jffi/CLObject", &clobject_class);
}
cl_object get_clobject_pointer(JNIEnv* env, jobject obj){
#if defined(INT_POINTER)
   clobject_pointer = get_cached_jfieldID(env, get_clobject_class(env), "pointer", "I", &clobject_pointer);
   return (cl_object) (*env)->GetIntField(env, obj, clobject_pointer);
#elif defined(LONG_POINTER)
   clobject_pointer = get_cached_jfieldID(env, get_clobject_class(env), "pointer", "J", &clobject_pointer);
   return (cl_object) (*env)->GetLongField(env, obj, clobject_pointer);
#endif
}

jobject return_clobject(JNIEnv *env, cl_object ret){
   clobject_class = get_clobject_class(env);
#if defined(INT_POINTER)
   clobject_constructor = get_cached_jmethodID(env, clobject_class, "<init>", "(I)V", &clobject_constructor);
   return (*env)->NewObject(env, clobject_class, clobject_constructor, (jint) ret);
#elif defined(LONG_POINTER)
   clobject_constructor = get_cached_jmethodID(env, clobject_class, "<init>", "(J)V", &clobject_constructor);
   return (*env)->NewObject(env, clobject_class, clobject_constructor, (jlong) ret);
#endif
}

static jclass boolean_class = NULL;
static jmethodID boolean_mID = NULL;
static jclass byte_class = NULL;
static jmethodID byte_mID = NULL;
static jclass char_class = NULL;
static jmethodID char_mID = NULL;
static jclass short_class = NULL;
static jmethodID short_mID = NULL;
static jclass int_class = NULL;
static jmethodID int_mID = NULL;
static jclass long_class = NULL;
static jmethodID long_mID = NULL;
static jclass float_class = NULL;
static jmethodID float_mID = NULL;
static jclass double_class = NULL;
static jmethodID double_mID = NULL;

cl_object type_conversion(JNIEnv* env, jobject obj){
   if((*env)->IsInstanceOf(env, obj, get_clobject_class(env)))
      return get_clobject_pointer(env, obj);
   jclass cboolean = get_cached_jclass(env, "java/lang/Boolean", &boolean_class);
   if((*env)->IsInstanceOf(env, obj, cboolean)){
      jboolean ret = (*env)->CallBooleanMethod(env, obj, get_cached_jmethodID(env, cboolean, "booleanValue", "()Z", &boolean_mID));
      throw_exception_if_occurred(env);
      if(ret == JNI_FALSE)
	 return ECL_NIL;
      return ECL_T;
   }
   jclass cbyte = get_cached_jclass(env, "java/lang/Byte", &byte_class);
   if((*env)->IsInstanceOf(env, obj, cbyte)){
      jbyte ret = (*env)->CallByteMethod(env, obj, get_cached_jmethodID(env, cbyte, "byteValue", "()B", &byte_mID));
      throw_exception_if_occurred(env);
      return ecl_make_int8_t(ret);
   }
   jclass cchar = get_cached_jclass(env, "java/lang/Character", &char_class);
   if((*env)->IsInstanceOf(env, obj, cchar)){
      jchar ret = (*env)->CallCharMethod(env, obj, get_cached_jmethodID(env, cchar, "charValue", "()C", &char_mID));
      throw_exception_if_occurred(env);
      return ecl_make_uint16_t(ret);
   }
   jclass cshort = get_cached_jclass(env, "java/lang/Short", &short_class);
   if((*env)->IsInstanceOf(env, obj, cshort)){
      jshort ret = (*env)->CallShortMethod(env, obj, get_cached_jmethodID(env, cshort, "shortValue", "()S", &short_mID));
      throw_exception_if_occurred(env);
      return ecl_make_int16_t(ret);
   }
   jclass cint = get_cached_jclass(env, "java/lang/Integer", &int_class);
   if((*env)->IsInstanceOf(env, obj, cint)){
      jint ret = (*env)->CallIntMethod(env, obj, get_cached_jmethodID(env, cint, "intValue", "()I", &int_mID));
      throw_exception_if_occurred(env);
      return ecl_make_int32_t(ret);
   }
   jclass clong = get_cached_jclass(env, "java/lang/Long", &long_class);
   if((*env)->IsInstanceOf(env, obj, clong)){
      jlong ret = (*env)->CallLongMethod(env, obj, get_cached_jmethodID(env, clong, "longValue", "()J", &long_mID));
      throw_exception_if_occurred(env);
      return ecl_make_int64_t(ret);
   }
   jclass cfloat = get_cached_jclass(env, "java/lang/Float", &float_class);
   if((*env)->IsInstanceOf(env, obj, cfloat)){
      jfloat ret = (*env)->CallFloatMethod(env, obj, get_cached_jmethodID(env, cfloat, "floatValue", "()F", &float_mID));
      throw_exception_if_occurred(env);
      return ecl_make_single_float(ret);
   }
   jclass cdouble = get_cached_jclass(env, "java/lang/Double", &double_class);
   if((*env)->IsInstanceOf(env, obj, cdouble)){
      jdouble ret = (*env)->CallDoubleMethod(env, obj, get_cached_jmethodID(env, cdouble, "doubleValue", "()D", &double_mID));
      throw_exception_if_occurred(env);
      return ecl_make_double_float(ret);
   }
   jobject global_obj;
   if(obj != NULL){
      global_obj = (*env)->NewGlobalRef(env, obj);
      (*env)->DeleteLocalRef(env, obj);
   }
   else
      global_obj = obj;
   cl_object ret = ecl_make_foreign_data((cl_object) ECL_FFI_POINTER_VOID, sizeof(jobject), global_obj);
   si_set_finalizer(ret, ecl_make_symbol("JAVA-DELETE-GLOBAL-REF", "JFFI"));
   return ret;
}

cl_object jobjectArray_to_list(JNIEnv *env, jobjectArray array){
   jsize length = (*env)->GetArrayLength(env, array);
   if(length == 0)
      return ECL_NIL;
   cl_object ret = ECL_NIL;
   for(jsize i = length - 1; i >= 0; i--){
      jobject obj = (*env)->GetObjectArrayElement(env, array, i);
      ret = cl_cons(type_conversion(env, obj), ret);
   }
   return ret;
}

jint java_attach_current_thread(){
   return (*java_vm)->AttachCurrentThread(java_vm, (void**) &java_env, NULL);
}

jint java_detach_current_thread(){
   return (*java_vm)->DetachCurrentThread(java_vm);
}

jint JNI_OnLoad(JavaVM* jvm, void* reserved){
   java_vm = jvm;
   return JNI_VERSION_1_2;
}

#define JFFI_NO_JVM_RUNNING_ERROR -1000
jint java_destroy_java_vm(){
   if(java_vm != NULL){
      jint ret = (*java_vm)->DestroyJavaVM(java_vm);
      java_vm = NULL;
      return ret;
   }
   return JFFI_NO_JVM_RUNNING_ERROR;
}

jboolean is_jvm_running(){
   if(java_vm == NULL)
      return JNI_FALSE;
   return JNI_TRUE;
}

void throw_lisp_condition(JNIEnv* env, cl_object condition){
   jclass lisp_condition_class = (*env)->FindClass(env, "jffi/LispCondition");
   if(lisp_condition_class == NULL)
      throw_exception(env);
   jmethodID lisp_condition_init = (*env)->GetMethodID(env, lisp_condition_class, "<init>", "(Ljffi/CLObject;)V");
   if(lisp_condition_init == NULL)
      throw_exception(env);
   jthrowable ret = (*env)->NewObject(env, lisp_condition_class, lisp_condition_init, return_clobject(env, condition));
   if(ret == NULL)
      throw_exception(env);
   (*env)->Throw(env, ret);
}

void throw_lisp_environment_error(JNIEnv* env, const char* message){
   jclass error_class = (*env)->FindClass(env, "jffi/LispEnvironmentError");
   if(error_class == NULL)
      throw_exception(env);
   (*env)->ThrowNew(env, error_class, message);
}

void set_global_lisp_variables(){
   apply_symbol = ecl_make_symbol("APPLY", "COMMON-LISP");
   reference_add_symbol = ecl_make_symbol("ADD-REFERENCE", "JFFI");
   error_list = cl_list(2, ecl_make_symbol("JAVA-EXCEPTION", "JFFI"), ecl_make_symbol("CONDITION", "COMMON-LISP"));
   jstring_conversion_symbol = ecl_make_symbol("CONVERT-TO-JSTRING", "JFFI");
   finalizer_key_symbol = ecl_make_keyword("FINALIZER");
   local_key_symbol = ecl_make_keyword("LOCAL");
}

cl_object funcall_symbol(JNIEnv* env, jstring packagename, jstring symbolname){
   const char* pname = (*env)->GetStringUTFChars(env, packagename, NULL);
   if(pname == NULL){
      throw_exception(env);
      return NULL;
   }
   const char* sname = (*env)->GetStringUTFChars(env, symbolname, NULL);
   if(sname == NULL){
      throw_exception(env);
      return NULL;
   }
   cl_env_ptr cl_env1 = ecl_process_env();
   cl_object symbol;
   cl_object found_condition = NULL;
   jthrowable found_exception = NULL;
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      symbol = ecl_make_symbol(sname, pname);
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->ReleaseStringUTFChars(env, packagename, pname);
      (*env)->ReleaseStringUTFChars(env, symbolname, sname);
      found_exception = (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION"));
   } ECL_HANDLER_CASE(2, condition){
      (*env)->ReleaseStringUTFChars(env, packagename, pname);
      (*env)->ReleaseStringUTFChars(env, symbolname, sname);
      found_condition = condition;
   } ECL_HANDLER_CASE_END;
   if(found_exception != NULL){
      (*env)->Throw(env, found_exception);
      return NULL;
   }
   if(found_condition != NULL){
      throw_lisp_condition(env, found_condition);
      return NULL;
   }
   (*env)->ReleaseStringUTFChars(env, packagename, pname);
   (*env)->ReleaseStringUTFChars(env, symbolname, sname);
   return symbol;
}

JNIEXPORT jobject JNICALL Java_jffi_Lisp_native_1funcall(JNIEnv *env, jclass lclass, jstring packagename, jstring symbolname, jobjectArray args){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return NULL;
   }
   java_env = env;
   cl_object symbol = funcall_symbol(env, packagename, symbolname);
   if(symbol == NULL)
      return NULL;
   jobject jret = NULL;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      cl_object list = jobjectArray_to_list(env, args);
      cl_object ret = cl_funcall(3, apply_symbol, symbol, list);
      cl_funcall(2, reference_add_symbol, ret);
      jret = return_clobject(env, ret);
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}

JNIEXPORT jobject JNICALL Java_jffi_Lisp_native_1object_1funcall(JNIEnv *env, jclass lclass, jstring packagename, jstring symbolname, jobjectArray args){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return NULL;
   }
   java_env = env;
   cl_object symbol = funcall_symbol(env, packagename, symbolname);
   if(symbol == NULL)
      return NULL;
   jobject jret = NULL;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      cl_object list = jobjectArray_to_list(env, args);
      cl_object ret = cl_funcall(3, apply_symbol, symbol, list);
      jret = (jobject) ecl_foreign_data_pointer_safe(ret);
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}

JNIEXPORT jobject JNICALL Java_jffi_Lisp_native_1read(JNIEnv* env, jclass lclass, jstring s){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return NULL;
   }
   const char* cs = (*env)->GetStringUTFChars(env, s, NULL);
   jobject jret = NULL;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      cl_object ret = c_string_to_object(cs);
      (*env)->ReleaseStringUTFChars(env, s, cs);
      jret = return_clobject(env, ret);
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->ReleaseStringUTFChars(env, s, cs);
      (*env)->Throw(env, (jthrowable) ecl_slot_value(java_exception, "POINTER"));
   } ECL_HANDLER_CASE(2, condition){
      (*env)->ReleaseStringUTFChars(env, s, cs);
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}

JNIEXPORT void JNICALL Java_jffi_CLObject_native_1finalize(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      if(!ecl_import_current_thread(ECL_NIL, ECL_NIL)){
	 throw_lisp_environment_error(env, "Can't attach the garbage collector's finalizer thread to ECL. CLObjects will not be garbage collected!");
	 return;
      }
      attached_to_lisp = 1;
   }
   cl_funcall(2, ecl_make_symbol("REMOVE-REFERENCE", "JFFI"), get_clobject_pointer(env, obj));
}

JNIEXPORT void JNICALL Java_jffi_CLObject_native_1unref(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return;
   }
   cl_funcall(2, ecl_make_symbol("REMOVE-REFERENCE", "JFFI"), get_clobject_pointer(env, obj));
}

void delete_global_references(JNIEnv* env){
   if(boolean_class != NULL)
      (*env)->DeleteGlobalRef(env, boolean_class);
   if(byte_class != NULL)
      (*env)->DeleteGlobalRef(env, byte_class);
   if(char_class != NULL)
      (*env)->DeleteGlobalRef(env, char_class);
   if(short_class != NULL)
      (*env)->DeleteGlobalRef(env, short_class);
   if(int_class != NULL)
      (*env)->DeleteGlobalRef(env, int_class);
   if(long_class != NULL)
      (*env)->DeleteGlobalRef(env, long_class);
   if(float_class != NULL)
      (*env)->DeleteGlobalRef(env, float_class);
   if(double_class != NULL)
      (*env)->DeleteGlobalRef(env, double_class);
   if(clobject_class != NULL)
      (*env)->DeleteGlobalRef(env, clobject_class);
}

void set_global_variables_null(){
   boolean_class = NULL;
   byte_class = NULL;
   char_class = NULL;
   short_class = NULL;
   int_class = NULL;
   long_class = NULL;
   float_class = NULL;
   double_class = NULL;
   clobject_class = NULL;
   boolean_mID = NULL;
   byte_mID = NULL;
   char_mID = NULL;
   short_mID = NULL;
   int_mID = NULL;
   long_mID = NULL;
   float_mID = NULL;
   double_mID = NULL;
   clobject_pointer = NULL;
   clobject_constructor = NULL;
   apply_symbol = NULL;
   reference_add_symbol = NULL;
   error_list = NULL;
   jstring_conversion_symbol = NULL;
   finalizer_key_symbol = NULL;
   local_key_symbol = NULL;
}

JNIEXPORT void JNICALL Java_jffi_Lisp_native_1stop(JNIEnv *env, jclass mclass){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return;
   }
   cl_funcall(1, ecl_make_symbol("DELETE-ALL-GLOBAL-REFERENCES", "JFFI"));
   si_gc(1, ECL_T);
   cl_shutdown();
   delete_global_references(env);
   java_env = NULL;
   set_global_variables_null();  
}

JNIEXPORT void JNICALL Java_jffi_Lisp_native_1attachCurrentThread(JNIEnv* env, jclass mclass){
   if(!attached_to_lisp){
      if(!ecl_import_current_thread(ECL_NIL, ECL_NIL)){
	 throw_lisp_environment_error(env, "Can't attach the current thread to ECL!");
	 return;
      }
      attached_to_lisp = 1;
      java_env = env;
   }
}
JNIEXPORT void JNICALL Java_jffi_Lisp_native_1detachCurrentThread(JNIEnv* env, jclass mclass){
   ecl_release_current_thread();
   attached_to_lisp = 0;
   java_env = NULL;
}

JNIEXPORT jstring JNICALL Java_jffi_CLObject_toString(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return NULL;
   }
   jobject jret = NULL;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      jret = (jstring) ecl_foreign_data_pointer_safe(cl_funcall(4, jstring_conversion_symbol, get_clobject_pointer(env, obj), finalizer_key_symbol, local_key_symbol));
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}
jobject Java_jffi_CLObject_toObject(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return NULL;
   }
   jobject jret = NULL;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      jret = (jobject) ecl_foreign_data_pointer_safe(get_clobject_pointer(env, obj));
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}
jboolean Java_jffi_CLObject_toBoolean(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return JNI_FALSE;
   }
   jboolean jret = JNI_FALSE;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      if(ecl_eql(get_clobject_pointer(env, obj), ECL_NIL))
	 jret = JNI_FALSE;
      else
	 jret = JNI_TRUE;
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}
jbyte Java_jffi_CLObject_toByte(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return 0;
   }
   jbyte jret = 0;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      jret = ecl_to_int8_t(get_clobject_pointer(env, obj));
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}
jchar Java_jffi_CLObject_toChar(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return 0;
   }
   jchar jret = 0;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      jret = ecl_to_uint16_t(get_clobject_pointer(env, obj));
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}
jshort Java_jffi_CLObject_toShort(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return 0;
   }
   jshort jret = 0;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      jret = ecl_to_int16_t(get_clobject_pointer(env, obj));
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}
jint Java_jffi_CLObject_toInt(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return 0;
   }
   jint jret = 0;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      jret = ecl_to_int32_t(get_clobject_pointer(env, obj));
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}
jlong Java_jffi_CLObject_toLong(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return 0;
   }
   jlong jret = 0;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      jret = ecl_to_int64_t(get_clobject_pointer(env, obj));
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}
jfloat Java_jffi_CLObject_toFloat(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return 0;
   }
   jfloat jret = 0;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      jret = ecl_to_float(get_clobject_pointer(env, obj));
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}
jdouble Java_jffi_CLObject_toDouble(JNIEnv* env, jobject obj){
   if(!attached_to_lisp){
      throw_lisp_environment_error(env, "This thread is not attached to the Lisp environment");
      return 0;
   }
   jdouble jret = 0;
   cl_env_ptr cl_env1 = ecl_process_env();
   ECL_HANDLER_CASE_BEGIN(cl_env1, error_list){
      jret = ecl_to_double(get_clobject_pointer(env, obj));
   } ECL_HANDLER_CASE(1, java_exception){
      (*env)->Throw(env, (jthrowable) ecl_foreign_data_pointer_safe(ecl_slot_value(java_exception, "JFFI::EXCEPTION")));
   } ECL_HANDLER_CASE(2, condition){
      throw_lisp_condition(env, condition);
   } ECL_HANDLER_CASE_END;
   return jret;
}

jvalue* jvalue_array_pointer(jvalue* array, unsigned int n){
   return array + n;
}
jthrowable java_test_exception(){
   jthrowable ret = (*java_env)->ExceptionOccurred(java_env);
   if(ret != NULL)
      (*java_env)->ExceptionClear(java_env);
   return ret;
}
void signal_java_exception(){
   jthrowable ex = (*java_env)->ExceptionOccurred(java_env);
   if(ex != NULL){
      (*java_env)->ExceptionClear(java_env);
      cl_error(1, cl_make_condition(3, ecl_read_from_cstring("JFFI::JAVA-EXCEPTION"), ecl_make_keyword("EXCEPTION"), ecl_make_pointer(ex)));
   }
}
void signal_lisp_condition(jthrowable exception){
   jclass lisp_condition_class = (*java_env)->FindClass(java_env, "jffi/LispCondition");
   if(lisp_condition_class == NULL)
      signal_java_exception();
   jmethodID get_clobject = (*java_env)->GetMethodID(java_env, lisp_condition_class, "getCondition", "()Ljffi/CLObject;");
   if(get_clobject == NULL)
      signal_java_exception();
   jobject java_clobject = (*java_env)->CallObjectMethod(java_env, exception, get_clobject);
   signal_java_exception();
   cl_error(1, get_clobject_pointer(java_env, java_clobject));
}


jboolean java_is_same_object(jobject obj1, jobject obj2){
   return (*java_env)->IsSameObject(java_env, obj1, obj2);
}
jclass java_new_global_ref(jobject obj){
   jclass ret = (jclass) (*java_env)->NewGlobalRef(java_env, obj);
   (*java_env)->DeleteLocalRef(java_env, obj);
   return ret;
}
void java_delete_global_ref(jobject obj){
   (*java_env)->DeleteGlobalRef(java_env, obj);
}
jobject java_new_local_ref(jobject obj){
   jobject ret = (jobject) (*java_env)->NewLocalRef(java_env, obj);
   (*java_env)->DeleteGlobalRef(java_env, obj);
   return ret;
}
void java_delete_local_ref(jobject obj){
   (*java_env)->DeleteLocalRef(java_env, obj);
}
jclass java_jclass(char* class_name){
   return (*java_env)->FindClass(java_env, class_name);
}
jmethodID java_jmethodID(jclass mclass, char* name, char* signature){
   return (*java_env)->GetMethodID(java_env, mclass, name, signature);
}
jmethodID java_static_jmethodID(jclass mclass, char* name, char* signature){
   return (*java_env)->GetStaticMethodID(java_env, mclass, name, signature);
}
jfieldID java_jfieldID(jclass mclass, char* name, char* signature){
   return (*java_env)->GetFieldID(java_env, mclass, name, signature);
}
jfieldID java_static_jfieldID(jclass mclass, char* name, char* signature){
   return (*java_env)->GetStaticFieldID(java_env, mclass, name, signature);
}
jmethodID java_constructorID(jclass mclass, char* signature){
   return (*java_env)->GetMethodID(java_env, mclass, "<init>", signature);
}

jobject java_constructor(jclass mclass, jmethodID mID, jvalue* args){
   return (*java_env)->NewObjectA(java_env, mclass, mID, args);
}

void java_void_method(jobject object, jmethodID mID, jvalue* args){
   (*java_env)->CallObjectMethodA(java_env, object, mID, args);
}
jobject java_object_method(jobject object, jmethodID mID, jvalue* args){
   return (*java_env)->CallObjectMethodA(java_env, object, mID, args);
}
jboolean java_boolean_method(jobject object, jmethodID mID, jvalue* args){
   return (*java_env)->CallBooleanMethodA(java_env, object, mID, args);
}
jbyte java_byte_method(jobject object, jmethodID mID, jvalue* args){
   return (*java_env)->CallByteMethodA(java_env, object, mID, args);
}
jchar java_char_method(jobject object, jmethodID mID, jvalue* args){
   return (*java_env)->CallCharMethodA(java_env, object, mID, args);
}
jshort java_short_method(jobject object, jmethodID mID, jvalue* args){
   return (*java_env)->CallShortMethodA(java_env, object, mID, args);
}
jint java_int_method(jobject object, jmethodID mID, jvalue* args){
   return (*java_env)->CallIntMethodA(java_env, object, mID, args);
}
jlong java_long_method(jobject object, jmethodID mID, jvalue* args){
   return (*java_env)->CallLongMethodA(java_env, object, mID, args);
}
jfloat java_float_method(jobject object, jmethodID mID, jvalue* args){
   return (*java_env)->CallFloatMethodA(java_env, object, mID, args);
}
jdouble java_double_method(jobject object, jmethodID mID, jvalue* args){
   return (*java_env)->CallDoubleMethodA(java_env, object, mID, args);
}

void java_static_void_method(jclass mjclass, jmethodID mID, jvalue* args){
   (*java_env)->CallStaticVoidMethodA(java_env, mjclass, mID, args);
}
jobject java_static_object_method(jclass mjclass, jmethodID mID, jvalue* args){
   return (*java_env)->CallStaticObjectMethodA(java_env, mjclass, mID, args);
}
jboolean java_static_boolean_method(jclass mjclass, jmethodID mID, jvalue* args){
   return (*java_env)->CallStaticBooleanMethodA(java_env, mjclass, mID, args);
}
jbyte java_static_byte_method(jclass mjclass, jmethodID mID, jvalue* args){
   return (*java_env)->CallStaticByteMethodA(java_env, mjclass, mID, args);
}
jchar java_static_char_method(jclass mjclass, jmethodID mID, jvalue* args){
   return (*java_env)->CallStaticCharMethodA(java_env, mjclass, mID, args);
}
jshort java_static_short_method(jclass mjclass, jmethodID mID, jvalue* args){
   return (*java_env)->CallStaticShortMethodA(java_env, mjclass, mID, args);
}
jint java_static_int_method(jclass mjclass, jmethodID mID, jvalue* args){
   return (*java_env)->CallStaticIntMethodA(java_env, mjclass, mID, args);
}
jlong java_static_long_method(jclass mjclass, jmethodID mID, jvalue* args){
   return (*java_env)->CallStaticLongMethodA(java_env, mjclass, mID, args);
}
jfloat java_static_float_method(jclass mjclass, jmethodID mID, jvalue* args){
   return (*java_env)->CallStaticFloatMethodA(java_env, mjclass, mID, args);
}
jdouble java_static_double_method(jclass mjclass, jmethodID mID, jvalue* args){
   return (*java_env)->CallStaticDoubleMethodA(java_env, mjclass, mID, args);
}

jobject java_object_field(jobject object, jfieldID mID){
   return (*java_env)->GetObjectField(java_env, object, mID);
}
jboolean java_boolean_field(jobject object, jfieldID mID){
   return (*java_env)->GetBooleanField(java_env, object, mID);
}
jbyte java_byte_field(jobject object, jfieldID mID){
   return (*java_env)->GetByteField(java_env, object, mID);
}
jchar java_char_field(jobject object, jfieldID mID){
   return (*java_env)->GetCharField(java_env, object, mID);
}
jshort java_short_field(jobject object, jfieldID mID){
   return (*java_env)->GetShortField(java_env, object, mID);
}
jint java_int_field(jobject object, jfieldID mID){
   return (*java_env)->GetIntField(java_env, object, mID);
}
jlong java_long_field(jobject object, jfieldID mID){
   return (*java_env)->GetLongField(java_env, object, mID);
}
jfloat java_float_field(jobject object, jfieldID mID){
   return (*java_env)->GetFloatField(java_env, object, mID);
}
jdouble java_double_field(jobject object, jfieldID mID){
   return (*java_env)->GetDoubleField(java_env, object, mID);
}

jobject java_static_object_field(jclass mjclass, jfieldID mID){
   return (*java_env)->GetStaticObjectField(java_env, mjclass, mID);
}
jboolean java_static_boolean_field(jclass mjclass, jfieldID mID){
   return (*java_env)->GetStaticBooleanField(java_env, mjclass, mID);
}
jbyte java_static_byte_field(jclass mjclass, jfieldID mID){
   return (*java_env)->GetStaticByteField(java_env, mjclass, mID);
}
jchar java_static_char_field(jclass mjclass, jfieldID mID){
   return (*java_env)->GetStaticCharField(java_env, mjclass, mID);
}
jshort java_static_short_field(jclass mjclass, jfieldID mID){
   return (*java_env)->GetStaticShortField(java_env, mjclass, mID);
}
jint java_static_int_field(jclass mjclass, jfieldID mID){
   return (*java_env)->GetStaticIntField(java_env, mjclass, mID);
}
jlong java_static_long_field(jclass mjclass, jfieldID mID){
   return (*java_env)->GetStaticLongField(java_env, mjclass, mID);
}
jfloat java_static_float_field(jclass mjclass, jfieldID mID){
   return (*java_env)->GetStaticFloatField(java_env, mjclass, mID);
}
jdouble java_static_double_field(jclass mjclass, jfieldID mID){
   return (*java_env)->GetStaticDoubleField(java_env, mjclass, mID);
}

void java_set_object_field(jobject object, jfieldID mID, jobject value){
   (*java_env)->SetObjectField(java_env, object, mID, value);
}
void java_set_boolean_field(jobject object, jfieldID mID, jboolean value){
   (*java_env)->SetBooleanField(java_env, object, mID, value);
}
void java_set_byte_field(jobject object, jfieldID mID, jbyte value){
   (*java_env)->SetByteField(java_env, object, mID, value);
}
void java_set_char_field(jobject object, jfieldID mID, jchar value){
   (*java_env)->SetCharField(java_env, object, mID, value);
}
void java_set_short_field(jobject object, jfieldID mID, jshort value){
   (*java_env)->SetShortField(java_env, object, mID, value);
}
void java_set_int_field(jobject object, jfieldID mID, jint value){
   (*java_env)->SetIntField(java_env, object, mID, value);
}
void java_set_long_field(jobject object, jfieldID mID, jlong value){
   (*java_env)->SetLongField(java_env, object, mID, value);
}
void java_set_float_field(jobject object, jfieldID mID, jfloat value){
   (*java_env)->SetFloatField(java_env, object, mID, value);
}
void java_set_double_field(jobject object, jfieldID mID, jdouble value){
   (*java_env)->SetDoubleField(java_env, object, mID, value);
}

void java_set_static_object_field(jclass mjclass, jfieldID mID, jobject value){
   (*java_env)->SetStaticObjectField(java_env, mjclass, mID, value);
}
void java_set_static_boolean_field(jclass mjclass, jfieldID mID, jboolean value){
   (*java_env)->SetStaticBooleanField(java_env, mjclass, mID, value);
}
void java_set_static_byte_field(jclass mjclass, jfieldID mID, jbyte value){
   (*java_env)->SetStaticByteField(java_env, mjclass, mID, value);
}
void java_set_static_char_field(jclass mjclass, jfieldID mID, jchar value){
   (*java_env)->SetStaticCharField(java_env, mjclass, mID, value);
}
void java_set_static_short_field(jclass mjclass, jfieldID mID, jshort value){
   (*java_env)->SetStaticShortField(java_env, mjclass, mID, value);
}
void java_set_static_int_field(jclass mjclass, jfieldID mID, jint value){
   (*java_env)->SetStaticIntField(java_env, mjclass, mID, value);
}
void java_set_static_long_field(jclass mjclass, jfieldID mID, jlong value){
   (*java_env)->SetStaticLongField(java_env, mjclass, mID, value);
}
void java_set_static_float_field(jclass mjclass, jfieldID mID, jfloat value){
   (*java_env)->SetStaticFloatField(java_env, mjclass, mID, value);
}
void java_set_static_double_field(jclass mjclass, jfieldID mID, jdouble value){
   (*java_env)->SetStaticDoubleField(java_env, mjclass, mID, value);
}

jsize array_length(jarray array){
   return (*java_env)->GetArrayLength(java_env, array);
}
jobjectArray java_new_object_array(jsize size, jclass mclass, jobject initial_element){
   return (*java_env)->NewObjectArray(java_env, size, mclass, initial_element);
}
jbooleanArray java_new_boolean_array(jsize size){
   return (*java_env)->NewBooleanArray(java_env, size);
}
jbyteArray java_new_byte_array(jsize size){
   return (*java_env)->NewByteArray(java_env, size);
}
jcharArray java_new_char_array(jsize size){
   return (*java_env)->NewCharArray(java_env, size);
}
jshortArray java_new_short_array(jsize size){
   return (*java_env)->NewShortArray(java_env, size);
}
jintArray java_new_int_array(jsize size){
   return (*java_env)->NewIntArray(java_env, size);
}
jlongArray java_new_long_array(jsize size){
   return (*java_env)->NewLongArray(java_env, size);
}
jfloatArray java_new_float_array(jsize size){
   return (*java_env)->NewFloatArray(java_env, size);
}
jdoubleArray java_new_double_array(jsize size){
   return (*java_env)->NewDoubleArray(java_env, size);
}

jobject java_get_object_array_elt(jobjectArray array, jsize index){
   return (*java_env)->GetObjectArrayElement(java_env, array, index);
}
jboolean java_get_boolean_array_elt(jbooleanArray array, jsize index){
   jboolean ret;
   (*java_env)->GetBooleanArrayRegion(java_env, array, index, 1, &ret);
   return ret;
}
jbyte java_get_byte_array_elt(jbyteArray array, jsize index){
   jbyte ret;
   (*java_env)->GetByteArrayRegion(java_env, array, index, 1, &ret);
   return ret;
}
jchar java_get_char_array_elt(jcharArray array, jsize index){
   jchar ret;
   (*java_env)->GetCharArrayRegion(java_env, array, index, 1, &ret);
   return ret;
}
jshort java_get_short_array_elt(jshortArray array, jsize index){
   jshort ret;
   (*java_env)->GetShortArrayRegion(java_env, array, index, 1, &ret);
   return ret;
}
jint java_get_int_array_elt(jintArray array, jsize index){
   jint ret;
   (*java_env)->GetIntArrayRegion(java_env, array, index, 1, &ret);
   return ret;
}
jlong java_get_long_array_elt(jlongArray array, jsize index){
   jlong ret;
   (*java_env)->GetLongArrayRegion(java_env, array, index, 1, &ret);
   return ret;
}
jfloat java_get_float_array_elt(jfloatArray array, jsize index){
   jfloat ret;
   (*java_env)->GetFloatArrayRegion(java_env, array, index, 1, &ret);
   return ret;
}
jdouble java_get_double_array_elt(jdoubleArray array, jsize index){
   jdouble ret;
   (*java_env)->GetDoubleArrayRegion(java_env, array, index, 1, &ret);
   return ret;
}

void java_fill_object_array_region(jobjectArray array, jsize index, jsize len, jobject x){
   for(jsize i = 0; i < len; i++)
      (*java_env)->SetObjectArrayElement(java_env, array, index, x);
}
void java_fill_boolean_array_region(jbooleanArray array, jsize index, jsize len, jboolean x){
   jboolean* y = malloc(len * sizeof(jboolean));
   for(jsize i = 0; i < len; i++)
      y[i] = x;
   (*java_env)->SetBooleanArrayRegion(java_env, array, index, len, y);
}
void java_fill_byte_array_region(jbyteArray array, jsize index, jsize len, jbyte x){
   jbyte* y = malloc(len * sizeof(jbyte));
   for(jsize i = 0; i < len; i++)
      y[i] = x;
   (*java_env)->SetByteArrayRegion(java_env, array, index, len, y);
}
void java_fill_char_array_region(jcharArray array, jsize index, jsize len, jchar x){
   jchar* y = malloc(len * sizeof(jchar));
   for(jsize i = 0; i < len; i++)
      y[i] = x;
   (*java_env)->SetCharArrayRegion(java_env, array, index, len, y);
}
void java_fill_short_array_region(jshortArray array, jsize index, jsize len, jshort x){
   jshort* y = malloc(len * sizeof(jshort));
   for(jsize i = 0; i < len; i++)
      y[i] = x;
   (*java_env)->SetShortArrayRegion(java_env, array, index, len, y);
}
void java_fill_int_array_region(jintArray array, jsize index, jsize len, jint x){
   jint* y = malloc(len * sizeof(jint));
   for(jsize i = 0; i < len; i++)
      y[i] = x;
   (*java_env)->SetIntArrayRegion(java_env, array, index, len, y);
}
void java_fill_long_array_region(jlongArray array, jsize index, jsize len, jlong x){
   jlong* y = malloc(len * sizeof(jlong));
   for(jsize i = 0; i < len; i++)
      y[i] = x;
   (*java_env)->SetLongArrayRegion(java_env, array, index, len, y);
}
void java_fill_float_array_region(jfloatArray array, jsize index, jsize len, jfloat x){
   jfloat* y = malloc(len * sizeof(jfloat));
   for(jsize i = 0; i < len; i++)
      y[i] = x;
   (*java_env)->SetFloatArrayRegion(java_env, array, index, len, y);
}
void java_fill_double_array_region(jdoubleArray array, jsize index, jsize len, jdouble x){
   jdouble* y = malloc(len * sizeof(jdouble));
   for(jsize i = 0; i < len; i++)
      y[i] = x;
   (*java_env)->SetDoubleArrayRegion(java_env, array, index, len, y);
}

jboolean* java_get_boolean_array_elements(jbooleanArray array){
   return (*java_env)->GetBooleanArrayElements(java_env, array, NULL);
}
jbyte* java_get_byte_array_elements(jbyteArray array){
   return (*java_env)->GetByteArrayElements(java_env, array, NULL);
}
jchar* java_get_char_array_elements(jcharArray array){
   return (*java_env)->GetCharArrayElements(java_env, array, NULL);
}
jshort* java_get_short_array_elements(jshortArray array){
   return (*java_env)->GetShortArrayElements(java_env, array, NULL);
}
jint* java_get_int_array_elements(jintArray array){
   return (*java_env)->GetIntArrayElements(java_env, array, NULL);
}
jlong* java_get_long_array_elements(jlongArray array){
   return (*java_env)->GetLongArrayElements(java_env, array, NULL);
}
jfloat* java_get_float_array_elements(jfloatArray array){
   return (*java_env)->GetFloatArrayElements(java_env, array, NULL);
}
jdouble* java_get_double_array_elements(jdoubleArray array){
   return (*java_env)->GetDoubleArrayElements(java_env, array, NULL);
}

void java_release_boolean_array_elements(jbooleanArray array, jboolean* carray){
   (*java_env)->ReleaseBooleanArrayElements(java_env, array, carray, 0);
}
void java_release_byte_array_elements(jbyteArray array, jbyte* carray){
   (*java_env)->ReleaseByteArrayElements(java_env, array, carray, 0);
}
void java_release_char_array_elements(jcharArray array, jchar* carray){
   (*java_env)->ReleaseCharArrayElements(java_env, array, carray, 0);
}
void java_release_short_array_elements(jshortArray array, jshort* carray){
   (*java_env)->ReleaseShortArrayElements(java_env, array, carray, 0);
}
void java_release_int_array_elements(jintArray array, jint* carray){
   (*java_env)->ReleaseIntArrayElements(java_env, array, carray, 0);
}
void java_release_long_array_elements(jlongArray array, jlong* carray){
   (*java_env)->ReleaseLongArrayElements(java_env, array, carray, 0);
}
void java_release_float_array_elements(jfloatArray array, jfloat* carray){
   (*java_env)->ReleaseFloatArrayElements(java_env, array, carray, 0);
}
void java_release_double_array_elements(jdoubleArray array, jdouble* carray){
   (*java_env)->ReleaseDoubleArrayElements(java_env, array, carray, 0);
}

jsize java_string_length(jstring string){
   return (*java_env)->GetStringLength(java_env, string);
}
jchar* java_string_char_array(jstring string){
   return (jchar*) (*java_env)->GetStringChars(java_env, string, NULL);
}
void java_string_release_char_array(jstring string, jchar* array){
   (*java_env)->ReleaseStringChars(java_env, string, array);
}
jstring java_new_string(jchar* array, jsize length){
   return (*java_env)->NewString(java_env, array, length);
}

jboolean java_instanceof(jobject obj, jclass mclass){
   return (*java_env)->IsInstanceOf(java_env, obj, mclass);
}
