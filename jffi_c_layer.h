// Copyright 2017 Marius Gerbershagen
// This file is part of JFFI.
//
//  JFFI is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  JFFI is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with JFFI.  If not, see <http://www.gnu.org/licenses/>.

#ifndef JFFI_C_LAYER_H
#define JFFI_C_LAYER_H

#define GC_THREADS
#define _REENTRANT
#include <ecl/ecl.h>
#include <jni.h>

extern JavaVM* java_vm;
extern __thread JNIEnv* java_env;
extern __thread char attached_to_lisp;
//if pseudo_start is true, then there is already a Lisp environment running
//and we don't need to call cl_boot() in Lisp.native_start()
extern char pseudo_start;

//Some often needed lisp objects are cached to avoid recomputing them
cl_object apply_symbol;
cl_object reference_add_symbol;
cl_object error_list;
cl_object jstring_conversion_symbol;
cl_object finalizer_key_symbol;
cl_object local_key_symbol;
void set_global_lisp_variables();

void throw_lisp_environment_error(JNIEnv* env, const char* message);

jint java_attach_current_thread();
jint java_detach_current_thread();
jint java_create_java_vm(jint n_options, char** options);
jint java_destroy_java_vm();
jboolean is_jvm_running();

//Address of the nth element in array, Needed, because ECL's ffi uses
//pointer arithmetic with lisp integers and (ffi:deref-array) returns
//the object at the nth element, but not the pointer to this element. 
jvalue* jvalue_array_pointer(jvalue* array, unsigned int n);
jthrowable java_test_exception();
void signal_lisp_condition(jthrowable exception);

jboolean java_is_same_object(jobject obj1, jobject obj2);
jclass java_new_global_ref(jobject obj);
void java_delete_global_ref(jobject obj);
jobject java_new_local_ref(jobject obj);
void java_delete_local_ref(jobject obj);
jclass java_jclass(char* class_name);
jmethodID java_jmethodID(jclass mclass, char* name, char* signature);
jmethodID java_static_jmethodID(jclass mclass, char* name, char* signature);
jfieldID java_jfieldID(jclass mclass, char* name, char* signature);
jfieldID java_static_jfieldID(jclass mclass, char* name, char* signature);
jmethodID java_constructorID(jclass mclass, char* signature);

jobject java_constructor(jclass mclass, jmethodID mID, jvalue* args);

void java_void_method(jobject object, jmethodID mID, jvalue* args);
jobject java_object_method(jobject object, jmethodID mID, jvalue* args);
jboolean java_boolean_method(jobject object, jmethodID mID, jvalue* args);
jbyte java_byte_method(jobject object, jmethodID mID, jvalue* args);
jchar java_char_method(jobject object, jmethodID mID, jvalue* args);
jshort java_short_method(jobject object, jmethodID mID, jvalue* args);
jint java_int_method(jobject object, jmethodID mID, jvalue* args);
jlong java_long_method(jobject object, jmethodID mID, jvalue* args);
jfloat java_float_method(jobject object, jmethodID mID, jvalue* args);
jdouble java_double_method(jobject object, jmethodID mID, jvalue* args);

void java_static_void_method(jclass mjclass, jmethodID mID, jvalue* args);
jobject java_static_object_method(jclass mjclass, jmethodID mID, jvalue* args);
jboolean java_static_boolean_method(jclass mjclass, jmethodID mID, jvalue* args);
jbyte java_static_byte_method(jclass mjclass, jmethodID mID, jvalue* args);
jchar java_static_char_method(jclass mjclass, jmethodID mID, jvalue* args);
jshort java_static_short_method(jclass mjclass, jmethodID mID, jvalue* args);
jint java_static_int_method(jclass mjclass, jmethodID mID, jvalue* args);
jlong java_static_long_method(jclass mjclass, jmethodID mID, jvalue* args);
jfloat java_static_float_method(jclass mjclass, jmethodID mID, jvalue* args);
jdouble java_static_double_method(jclass mjclass, jmethodID mID, jvalue* args);

jobject java_object_field(jobject object, jfieldID mID);
jboolean java_boolean_field(jobject object, jfieldID mID);
jbyte java_byte_field(jobject object, jfieldID mID);
jchar java_char_field(jobject object, jfieldID mID);
jshort java_short_field(jobject object, jfieldID mID);
jint java_int_field(jobject object, jfieldID mID);
jlong java_long_field(jobject object, jfieldID mID);
jfloat java_float_field(jobject object, jfieldID mID);
jdouble java_double_field(jobject object, jfieldID mID);

jobject java_static_object_field(jclass mjclass, jfieldID mID);
jboolean java_static_boolean_field(jclass mjclass, jfieldID mID);
jbyte java_static_byte_field(jclass mjclass, jfieldID mID);
jchar java_static_char_field(jclass mjclass, jfieldID mID);
jshort java_static_short_field(jclass mjclass, jfieldID mID);
jint java_static_int_field(jclass mjclass, jfieldID mID);
jlong java_static_long_field(jclass mjclass, jfieldID mID);
jfloat java_static_float_field(jclass mjclass, jfieldID mID);
jdouble java_static_double_field(jclass mjclass, jfieldID mID);

void java_set_object_field(jobject object, jfieldID mID, jobject value);
void java_set_boolean_field(jobject object, jfieldID mID, jboolean value);
void java_set_byte_field(jobject object, jfieldID mID, jbyte value);
void java_set_char_field(jobject object, jfieldID mID, jchar value);
void java_set_short_field(jobject object, jfieldID mID, jshort value);
void java_set_int_field(jobject object, jfieldID mID, jint value);
void java_set_long_field(jobject object, jfieldID mID, jlong value);
void java_set_float_field(jobject object, jfieldID mID, jfloat value);
void java_set_double_field(jobject object, jfieldID mID, jdouble value);

void java_set_static_object_field(jclass mjclass, jfieldID mID, jobject value);
void java_set_static_boolean_field(jclass mjclass, jfieldID mID, jboolean value);
void java_set_static_byte_field(jclass mjclass, jfieldID mID, jbyte value);
void java_set_static_char_field(jclass mjclass, jfieldID mID, jchar value);
void java_set_static_short_field(jclass mjclass, jfieldID mID, jshort value);
void java_set_static_int_field(jclass mjclass, jfieldID mID, jint value);
void java_set_static_long_field(jclass mjclass, jfieldID mID, jlong value);
void java_set_static_float_field(jclass mjclass, jfieldID mID, jfloat value);
void java_set_static_double_field(jclass mjclass, jfieldID mID, jdouble value);

jsize array_length(jarray array);
jobjectArray java_new_object_array(jsize size, jclass mclass, jobject initial_element);
jbooleanArray java_new_boolean_array(jsize size);
jbyteArray java_new_byte_array(jsize size);
jcharArray java_new_char_array(jsize size);
jshortArray java_new_short_array(jsize size);
jintArray java_new_int_array(jsize size);
jlongArray java_new_long_array(jsize size);
jfloatArray java_new_float_array(jsize size);
jdoubleArray java_new_double_array(jsize size);

jobject java_get_object_array_elt(jobjectArray array, jsize index);
jboolean java_get_boolean_array_elt(jbooleanArray array, jsize index);
jbyte java_get_byte_array_elt(jbyteArray array, jsize index);
jchar java_get_char_array_elt(jcharArray array, jsize index);
jshort java_get_short_array_elt(jshortArray array, jsize index);
jint java_get_int_array_elt(jintArray array, jsize index);
jlong java_get_long_array_elt(jlongArray array, jsize index);
jfloat java_get_float_array_elt(jfloatArray array, jsize index);
jdouble java_get_double_array_elt(jdoubleArray array, jsize index);

void java_fill_object_array_region(jobjectArray array, jsize index, jsize len, jobject x);
void java_fill_boolean_array_region(jbooleanArray array, jsize index, jsize len, jboolean x);
void java_fill_byte_array_region(jbyteArray array, jsize index, jsize len, jbyte x);
void java_fill_char_array_region(jcharArray array, jsize index, jsize len, jchar x);
void java_fill_short_array_region(jshortArray array, jsize index, jsize len, jshort x);
void java_fill_int_array_region(jintArray array, jsize index, jsize len, jint x);
void java_fill_long_array_region(jlongArray array, jsize index, jsize len, jlong x);
void java_fill_float_array_region(jfloatArray array, jsize index, jsize len, jfloat x);
void java_fill_double_array_region(jdoubleArray array, jsize index, jsize len, jdouble x);

jboolean* java_get_boolean_array_elements(jbooleanArray array);
jbyte* java_get_byte_array_elements(jbyteArray array);
jchar* java_get_char_array_elements(jcharArray array);
jshort* java_get_short_array_elements(jshortArray array);
jint* java_get_int_array_elements(jintArray array);
jlong* java_get_long_array_elements(jlongArray array);
jfloat* java_get_float_array_elements(jfloatArray array);
jdouble* java_get_double_array_elements(jdoubleArray array);

void java_release_boolean_array_elements(jbooleanArray array, jboolean* carray);
void java_release_byte_array_elements(jbyteArray array, jbyte* carray);
void java_release_char_array_elements(jcharArray array, jchar* carray);
void java_release_short_array_elements(jshortArray array, jshort* carray);
void java_release_int_array_elements(jintArray array, jint* carray);
void java_release_long_array_elements(jlongArray array, jlong* carray);
void java_release_float_array_elements(jfloatArray array, jfloat* carray);
void java_release_double_array_elements(jdoubleArray array, jdouble* carray);

jsize java_string_length(jstring string);
jchar* java_string_char_array(jstring string);
void java_string_release_char_array(jstring string, jchar* array);
jstring java_new_string(jchar* array, jsize length);

jboolean java_instanceof(jobject obj, jclass mclass);

#endif /* JFFI_C_LAYER_H */
